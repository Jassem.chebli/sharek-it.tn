import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-usernavbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public isCollapsed = true;
  @Output() onchangeLang = new EventEmitter<string>();
  changeLang(language: string) {
    this.onchangeLang.emit(language);
  }

  constructor(
    public translate: TranslateService) {

  }

  ngOnInit(): void {
  }

}
