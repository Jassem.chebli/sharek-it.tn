export class DONARGENT {
    _id: String;
    nom: String;
    prenom: String;
    email: String;
    montant: String;
    montantlettre: String;
    file: String;
    raisonsociale: String;
    identifiant: String;
    adresse: String;
    codepostal: String;
    commune: String;
    type: String;

}
