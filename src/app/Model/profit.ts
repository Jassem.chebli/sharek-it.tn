export class Profit {

    public _id:String;
    public nom: String;
    public prenom:String;
    public ville: String;
    public adresse: String;
    public tel: Number;
    public Email: String;
    public type: String;
    public nom_universite: String;
    public niveau_scolarite: String;
    public specialite: String;
    public etablissement : String ; 
    public codepostal : String ; 
    public besoin: String;
    public detail : String 
    public Image: String;
    public produit_profite: String[];
    public etat:String
    constructor(){}
}
