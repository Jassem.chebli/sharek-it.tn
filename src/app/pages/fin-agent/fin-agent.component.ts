import { Component, OnInit } from '@angular/core';
import { DonArgentService } from 'src/app/_services/donArgent/don-argent.service';
import { DONARGENT } from 'src/app/Model/donArgent';

@Component({
  selector: 'app-fin-agent',
  templateUrl: './fin-agent.component.html',
  styleUrls: ['./fin-agent.component.css']
})
export class FinAgentComponent implements OnInit {
  dons: DONARGENT[] = []
  settings = {
    actions: { edit: false, add: false, delete: false },
    delete: {

      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      email: {
        title: 'Email',

      },
      montant: {
        title: 'Montant',

      }
    }
  };
  constructor(private DonsArgentService: DonArgentService) { }

  ngOnInit(): void {
    this.DonsArgentService.Afficher().subscribe(res => {
      this.dons = res;
    })
  }

}
