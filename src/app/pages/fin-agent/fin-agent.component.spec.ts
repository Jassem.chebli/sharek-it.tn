import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinAgentComponent } from './fin-agent.component';

describe('FinAgentComponent', () => {
  let component: FinAgentComponent;
  let fixture: ComponentFixture<FinAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
