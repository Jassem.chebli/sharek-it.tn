import { Component, OnInit } from '@angular/core';
import { MatchN } from 'src/app/Model/MatchN';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { DonService } from 'src/app/_services/Don/don.service';
import { SnotifyService } from 'ng-snotify';
import { MatchNService } from 'src/app/_services/MatchN/match-n.service';
import { ProfitService } from 'src/app/_services/Profit/profit.service';
import { NgbdModalContent } from './modal.component';

@Component({
  selector: 'app-matchesnew',
  templateUrl: './matchesnew.component.html',
  styleUrls: ['./matchesnew.component.css']
})
export class MatchesnewComponent implements OnInit {
  matchs: MatchN[] = []
  data = [];
  Fdata =  [];

  settings = {
    actions: { edit: false, add: false },
    delete: {
      confirmDelete: true,

      deleteButtonContent: 'Supprimer',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      etat: {
        title: 'Nom',


      }
    }
  };
  cl
  etat
  profit
  donneur
  ob={}
  constructor(private modalService: NgbModal , private router: Router, private donservice: DonService, private route: ActivatedRoute
    , private profitService: ProfitService, private matchsService: MatchNService , private snotifyService: SnotifyService) {

  }
  ngOnInit(): void {
    this.matchsService.Afficher().subscribe(x=>{
       this.data=x;
       console.log(this.data);

    })
  }
  funct(event: any) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.data = event;
  }
  
  delete(va){

    this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {text: 'Yes', action: (toast) =>        { 
          this.matchsService.Supprimer(va._id).subscribe(res=>{this.data.splice(this.data.indexOf(va), 1);

          
          this.snotifyService.success('Success', {
          timeout: 2000,
          showProgressBar: false,
          closeOnClick: false,
          pauseOnHover: true
        })},
        err=>{
          this.snotifyService.error('Error', {
            timeout: 2000,
            showProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true
          })
        })
        
        
        ;this.snotifyService.remove(toast.id)}
        , bold: false},
        {text: 'No', action: (toast) =>        {this.snotifyService.remove(toast.id)}},
       
        {text: 'Close', action: (toast) => { this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
   


  }
  chnage(dat){
   dat.idProfit.etat=dat.etat ;
   dat.idDon.statut=dat.etat
    
    this.matchsService.Modifier(dat).subscribe(res=>{console.log(res);
     
    this.snotifyService.success('Success', {
      timeout: 2000,
      showProgressBar: false,
      closeOnClick: false,
      pauseOnHover: true
    })},
    err=>{
      console.log(err);
      this.snotifyService.error('Error', {
        timeout: 2000,
        showProgressBar: false,
        closeOnClick: false,
        pauseOnHover: true
      })
    }
    )
    
    
    
      }
}
