import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header" >
      <h4 class="modal-title">Fiche</h4>
      <p>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div class="row">
      <div class="col">
      <h3>DONATEUR</h3>
      <br>
    <h5>Informations</h5>
    
    Nom : {{data.idDon.nom}} <br>
    Prenom : {{data.idDon.prenom}} <br>
    Age :{{data.idDon.age}} <br> 
    CIN: {{data.idDon.cin}} <br>
    TeL :{{data.idDon.tel}} <br>
    Email :{{data.idDon.Email}} <br> <br>
    <h5>Coordonnées</h5>
    Ville:{{data.idDon.ville}} <br>
    adresse: {{data.idDon.adresse}} <br>
    Code postale :{{data.idDon.codepostal}} <br> <br>
    
    <h5>Produit</h5><br>
       Nom de produit : {{data.idDon.produitName}} <br>
    Detail : {{data.idDon.decriptionProduit}} <br>
    Etat : {{data.idDon.etat}} <br>
    message:{{data.idDon.message}} <br>  
    code Trustit:{{data.idDon.code_trustit}} <br>
    Modele:{{data.idDon.modele}} <br>
    IMEI_SERIAL:{{data.idDon.IMEI_SERIAL}} <br>
    diagnostic:{{data.idDon.diagnostic}} <br>
    <br>
    <img src= "{{ 'https://sharek-it.tn/' + data.idDon.ImageProduct }}"  class="rounded-top" style = "width:150px;">
    <br>   
     
    </div>
    <div class="col" >
    <h3>BÉNÉFICIAIRE</h3>
    <br>
    <h5>Informations</h5>
    Nom : {{data.idProfit.nom}} <br>
    Prenom : {{data.idProfit.prenom}} <br>
    Age :{{data.idProfit.age}} <br> 
    CIN: {{data.idProfit.cin}} <br>
    TeL :{{data.idProfit.tel}} <br>
    Email :{{data.idProfit.Email}} <br> 
    etablisement :{{data.idProfit.etablissement}} <br> 
    Nom du luniversitè : {{data.idProfit.nom_universite}} <br> 
    specialite   : {{data.idProfit.specialite }}     <br>    <br> 
    <h5>Coordonnées</h5>
    Ville:{{data.idProfit.ville}} <br>
    adresse: {{data.idProfit.adresse}} <br>
    Code postale :{{data.idProfit.codepostal}} <br> <br>
    
    <h5>Le Besoin</h5>
    
    Le Besoin : {{data.idProfit.besoin}} <br> 
    Detail du besoin : {{data.idProfit.detail}}
   <br>
   <br>
    <h2>Statut: {{data.etat}} </h2>

    </div>
    </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() data;
 
  constructor(public activeModal: NgbActiveModal) {
console.log(this.data)


  }
  chnage(va){

    
  }
  
}