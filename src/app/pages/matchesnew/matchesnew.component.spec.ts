import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchesnewComponent } from './matchesnew.component';

describe('MatchesnewComponent', () => {
  let component: MatchesnewComponent;
  let fixture: ComponentFixture<MatchesnewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchesnewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchesnewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
