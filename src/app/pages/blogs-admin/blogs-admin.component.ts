import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/_services/Blog/blog.service';
import { Blog } from 'src/app/Model/Blog';
import { Router } from '@angular/router';
import { SnotifyService } from 'ng-snotify';

@Component({
  selector: 'app-blogs-admin',
  templateUrl: './blogs-admin.component.html',
  styleUrls: ['./blogs-admin.component.css']
})
export class BlogsAdminComponent implements OnInit {
  posts: Blog[]

  settings = {
    actions: { edit: false, add: false },
    delete: {
      confirmDelete: true,

      deleteButtonContent: 'Supprimer',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      writer: {
        title: 'writer',
        valuePrepareFunction: (writer) => {
            return writer.name;
        }
        

      },
      title: {
        title: 'titre',

      }
    }
  };
  constructor( private BlogService: BlogService ,  private snotifyService: SnotifyService, private router: Router  ) { }

  ngOnInit(): void {
    this.BlogService.afficherPosts().subscribe(res => {this.posts=res.result ; console.log(res.result)})
  }
  funct(event){
    console.log(event.data )
    this.router.navigate(["/Post",event.data._id])
  }
onDeleteConfirm(event){

  console.log(event.data )
  
  this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
    timeout: 5000,
    showProgressBar: true,
    closeOnClick: false,
    pauseOnHover: true,
    buttons: [
      {
        text: 'Yes', action: (toast) => {



          this.BlogService.supprimer(event.data._id).subscribe(res => {
            this.snotifyService.success('Success', {
              timeout: 2000,
              showProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true
            })
          })


            ; this.snotifyService.remove(toast.id); event.confirm.resolve()
        }
        , bold: false
      },
      { text: 'No', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject() } },

      { text: 'Close', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject(); }, bold: true },
    ]
  });
    
}
}
