import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import ImageResize from 'quill-image-resize-module';
import * as QuillNamespace from 'quill';
import { BlogService } from 'src/app/_services/Blog/blog.service';
import { Blog } from 'src/app/Model/Blog';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-blog-creator',
  templateUrl: './blog-creator.component.html',
  styleUrls: ['./blog-creator.component.css']
})
export class BlogCreatorComponent implements OnInit {
  blog= new Blog
  editorForm: FormGroup;
  constructor(private http: HttpClient, private BlogService: BlogService, private tokenStorageService: TokenStorageService, private router: Router ) {
    let Quill: any = QuillNamespace;
    Quill.register('modules/imageResize', ImageResize);
  }
  quillEditorRef;
  maxUploadFileSize = 1000000;
  config = {
    imageResize: {
      displayStyles: {
        backgroundColor: 'black',
        border: 'none',
        color: 'white'
      },
      modules: ['Resize']
    }
  }
  ngOnInit(): void {
    this.blog.writer = this.tokenStorageService.getUser().user._id
    this.editorForm = new FormGroup({
      'editor': new FormControl(null),
      'title': new FormControl("",Validators.required)

    })
  }
  onSubmit() {
    console.log(this.editorForm.get('editor'))
    this.blog.content = this.editorForm.get('editor').value
    this.BlogService.createPost(this.blog).subscribe(res=>{   this.router.navigate(["/Post",res.postInfo._id])}, err =>console.log(err))
  }

  getEditorInstance(editorInstance: any) {
    this.quillEditorRef = editorInstance;
    console.log(this.quillEditorRef)
    const toolbar = editorInstance.getModule('toolbar');
    toolbar.addHandler('image', this.imageHandler);
  }
  _returnedURL
  imageHandler = (image, callback) => {
    const input = <HTMLInputElement>document.getElementById('fileInputField');
    document.getElementById('fileInputField').onchange = () => {
      let file: File;
      file = input.files[0];
      // file type is only image.
      if (/^image\//.test(file.type)) {

        const fd = new FormData()
        // just add file instance to form data normally
        fd.append('file', file)
        this.http.post<any>('https://sharek-it-back.herokuapp.com/api/Blog/uploadfiles', fd).subscribe(res => {
          console.log(res)

          const range = this.quillEditorRef.getSelection();
          const img = '<img style="max-height:100%" src="' + "https://sharek-it.tn/" + res.url + '" />';
          this.quillEditorRef.clipboard.dangerouslyPasteHTML(range.index, img);
          
          const img2 = '<p></p>';
          this.quillEditorRef.clipboard.dangerouslyPasteHTML(range.index, img2);
        })



      } else {
        alert('You could only upload images.');
      }
    };

    input.click();
  }

  pushImageToEditor() {


  }
}

