import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { ProfitService } from 'src/app/_services/Profit/profit.service';
import { profit } from 'src/app/Model/ProfitD';
import { DonService } from 'src/app/_services/Don/don.service';
import { donneur } from 'src/app/Model/Donneur';
import { MatchingService } from 'src/app/_services/matching/matching.service';

@Component({
  selector: 'app-mes-actions',
  templateUrl: './mes-actions.component.html',
  styleUrls: ['./mes-actions.component.css']
})
export class MesActionsComponent implements OnInit {
  data = new profit;
  panelOpenState = false;
  HasDons = true;
  dons: donneur[] = [];
  HasDemand = false;
  DemandeStatut = 'Waiting'
  constructor(private matchsService: MatchingService, private userService: TokenStorageService, private profitSerice: ProfitService, private donnerService: DonService) {



  }

  ngOnInit(): void {

 /*    console.log(this.userService.getUser().user)
    this.profitSerice.RechercherByUser(this.userService.getUser().user._id).subscribe(res => {
      this.data = res[0];
      if (res.length == 1) {
        this.HasDemand = true
        this.matchsService.RechercherByProfit(this.data._id).subscribe(res => {

          if (res.length > 0) {

            this.DemandeStatut = res[0].etat;
          }


        })
      }
    });

 */

    this.donnerService.ListDonByID(this.userService.getUser().user._id).subscribe(res => {
      this.dons = res;
    
      if (this.dons.length == 0)
        this.HasDons = false;
      else {
        this.dons.forEach(element => {
          this.matchsService.RechercherByDon(element._id as string).subscribe(x => {

         
           let a = "Waiting"

            if (x.length > 0) {
              a = x[0].etat;
              element.statut=a;

         
            }else{
              element.statut=a
            }


          });

          // this.dons[i].statut=this.a;
          /*                 Object.assign(this.dons[i],{statut:this.a})
           */







        });

      }
    });



    }

  }