import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DONARGENT } from 'src/app/Model/donArgent';
import { NgForm, FormControl, Validators, FormGroupDirective, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { DonArgentService } from 'src/app/_services/donArgent/don-argent.service';
import { SnotifyService } from 'ng-snotify';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from './SuccessModal.component';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { SignaturePad } from 'angular2-signaturepad';
import { TranslateService } from '@ngx-translate/core';
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-don-argent',
  templateUrl: './don-argent.component.html',
  styleUrls: ['./don-argent.component.css']

})

export class DonArgentComponent implements OnInit, AfterViewInit {
  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'canvasWidth': 500,
    'canvasHeight': 300,
    'penColor': "rgb(52, 42, 111)"
  };
  loading = false;
  today = new Date();
  dd = String(this.today.getDate()).padStart(2, '0');
  mm = String(this.today.getMonth() + 1).padStart(2, '0'); //January is 0!
  yyyy = this.today.getFullYear();

  cd = this.dd + '/' + this.mm + '/' + this.yyyy;
  donneur = new DONARGENT;
  userForm: FormGroup;
  statusClass = '';
  showConfirmer = true;

  constructor(private modalService: NgbModal,
    private router: Router,
    private fb: FormBuilder,
    private DonArgentService: DonArgentService,
    private snotifyService: SnotifyService,
    public translate: TranslateService) {
    this.initForm();
  }

  show = false;
  matcher = new MyErrorStateMatcher();
  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
  }
  ngOnInit(): void {
    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
  }
  onSubmit(f: NgForm) {
    console.log(f);  // { first: '', last: '' }
    console.log(f.invalid);  // false
    console.log(this.donneur);
    console.log(this.matcher);
    this.DonArgentService.Ajouter(this.donneur).subscribe(res => {
      this.snotifyService.success('votre demande a été prise en compte');
      this.router.navigate(['/']);

    }
      , err => {
        this.snotifyService.error(err);
        this.router.navigate(['/']);
      },
      () => {
        const modalRef = this.modalService.open(NgbdModalContent);
        modalRef.componentInstance.data = this.donneur.nom + ' ' + this.donneur.prenom;
      });


  }

  openmodal() {

    let data = document.getElementById('signatureCanvas');
    html2canvas(data, { allowTaint: true, scrollX: 0, scrollY: -window.scrollY }).then((canvas) => {
      // Few necessary setting options
      /* let imgWidth = 50;
      let pageHeight = 50;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      let heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL('image/png'); */
      let doc = document.getElementById('sig');
      doc.append(canvas)
      this.show = !this.show;
    });
    /* const canvas = <HTMLCanvasElement>document.getElementById('canvas2');
    const ctx = canvas.getContext('2d');
    const img = document.createElement('img');
    img.src = '';
    img.src = this.signaturePad.toDataURL();
    ctx.drawImage(img, 10, 10);
    setTimeout(() => {
      this.show = !this.show;
    }, 1000); */

  }
  initForm() {
    this.userForm = this.fb.group({
      emailFormControl: new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      nomFormControl: new FormControl('', [
        Validators.required,
      ]),
      prenomFormControl: new FormControl('', [
        Validators.required,
      ]),
      raisonsocialeFormControl: new FormControl('', [
        Validators.required,
      ]),
      identifiantFormControl: new FormControl('', [
        Validators.required,
      ]),
      adresseFormControl: new FormControl('', [
        Validators.required,
      ]),
      codepostalFormControl: new FormControl('', [
        Validators.required,
      ]),
      communeFormControl: new FormControl('', [
        Validators.required,
      ]),
      typeFormControl: new FormControl('', [
        Validators.required,
      ]),
      montantFormControl: new FormControl('', [
        Validators.required,
        Validators.min(1)
      ]),
      montantlettreFormControl: new FormControl('', [
        Validators.required,
      ])
    });
  }
  /* generatePDF() {
    const data = document.getElementById('contentToConvert');
    html2canvas(data).then(canvas => {
      const imgWidth = 208;
      const imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png');
      const pdf = new jspdf('p', 'mm', 'a4');
      const position = 0.5;
      pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
      pdf.save('test.pdf');
    });
  } */
  generatePDF() {
    this.showConfirmer = false;
    setTimeout(() => {
      let data = document.getElementById('contentToConvert');
      html2canvas(data, { allowTaint: true, scrollX: 0, scrollY: -window.scrollY }).then((canvas) => {
        // Few necessary setting options
        let imgWidth = 208;
        let pageHeight = 295;
        let imgHeight = canvas.height * imgWidth / canvas.width;
        let heightLeft = imgHeight;

        const contentDataURL = canvas.toDataURL('image/png');
        const pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF
        let position = 0;
        pdf.addImage(contentDataURL, 'PNG', 1, 1, imgWidth, pageHeight);


        setTimeout(function () {
          pdf.save("ttt.pdf")
        }, 4000);
      });
    }, 1000);
  }
  clearSig() {
    const myNode = document.getElementById("sig");
    myNode.innerHTML = '';
  }
  ngAfterViewInit() {
    this.signaturePad.clear();
  }
}
