import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header" >
      <h4 class="modal-title">Success</h4>
     
    </div>
    <div class="modal-body " style="text-align:center" >
    <i class="fas fa-check-circle fa-5x"></i>

    <br>
    <br>
    <h4>Merci {{data}}</h4>
      <p>votre demande a été prise en compte</p>
      
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() data;

  constructor(public activeModal: NgbActiveModal) {}
}