import { Component, OnInit } from '@angular/core';
import { COORDINATEUR } from '../../Model/coordinateur';
import { CoordinationService } from '../../_services/coordination/coordination.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-coordination',
  templateUrl: './coordination.component.html',
  styleUrls: ['./coordination.component.css']
})
export class CoordinationComponent implements OnInit {
  coordinateur = new COORDINATEUR();
  cord: COORDINATEUR[] = [];
  image: any;
  constructor(private http: HttpClient, private coordinateurservice: CoordinationService) {
    this.affiche();
  }
  affiche() {
    this.coordinateurservice.Afficher().subscribe(res => {
      this.cord = res;
    });
  }
  delete(p) {
    this.coordinateurservice.Supprimer(p._id).subscribe(res => {
      this.affiche();
    });
  }
  selectImage(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.image = file;
    }
  }
  onSubmit() {
    const formData = new FormData();
    formData.append('ImageProduct', this.image);
    this.http.post<any>('https://sharek-it-back.herokuapp.com/api/coordinateur/file', formData).
      subscribe(res => {
        this.coordinateur.image = 'assets/img/Coordinateur/' + res.originalname;
        this.coordinateurservice.Ajouter(this.coordinateur).subscribe(data => {
          this.affiche();
        });
      });
  }
  ngOnInit(): void { }

}
