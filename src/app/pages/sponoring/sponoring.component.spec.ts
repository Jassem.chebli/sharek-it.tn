import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SponoringComponent } from './sponoring.component';

describe('SponoringComponent', () => {
  let component: SponoringComponent;
  let fixture: ComponentFixture<SponoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SponoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SponoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
