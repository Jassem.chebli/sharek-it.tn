import { Component, OnInit } from '@angular/core';
import { SponsoringService } from 'src/app/_services/Sponsoring/sponsoring.service';
import { Sponsoring } from '../../Model/sponsoring';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Matching } from 'src/app/Model/Matching';
import { MatchingService } from 'src/app/_services/matching/matching.service';
@Component({
  selector: 'app-sponoring',
  templateUrl: './sponoring.component.html',
  styleUrls: ['./sponoring.component.css']
})
export class SponoringComponent implements OnInit {
  sponsor: Sponsoring[] = [];
  image: any;
  spon = new Sponsoring();
  matching = new Matching();
  constructor(private sponsorService: SponsoringService,
    private serviceMatching: MatchingService,
    private fb: FormBuilder,
    private http: HttpClient) {
    this.affiche();
  }

  ngOnInit(): void { }
  delete(p) {
    console.log(p._id);
    this.sponsorService.Supprimer(p._id).subscribe(res => {
      console.log("sponsor tfasa5");
      this.affiche();
    })
  }
  selectImage(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.image = file;
    }
  }
  onSubmit() {
    const formData = new FormData();
    formData.append('ImageProduct', this.image);
    this.http.post<any>('https://sharek-it-back.herokuapp.com/api/sponsorings/file', formData).
      subscribe(res => {
        this.spon.image = 'assets/img/Sponsoring/' + res.originalname;
        this.sponsorService.Ajouter(this.spon).subscribe(data => {
          console.log(data);
          this.affiche();
        });

      });
  }
  affiche() {
    this.sponsorService.Afficher().subscribe(res => {
      this.sponsor = res;
      console.log(this.sponsor);
    });
  }
  matchings() {
    console.log(this.matching);
    this.serviceMatching.Ajouter(this.matching).subscribe(res => {
      console.log(res);
    });
  }
}
