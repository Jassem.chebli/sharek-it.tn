import { Component, OnInit } from '@angular/core';
import { Donateur } from '../../Model/Donateur';
import { DonateurService } from '../../_services/donateur/donateur.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-donateur',
  templateUrl: './donateur.component.html',
  styleUrls: ['./donateur.component.css']
})
export class DonateurComponent implements OnInit {
  donateur = new Donateur();
  don: Donateur[] = [];
  image: any;
  constructor(private http: HttpClient, private donateurService: DonateurService) { }

  ngOnInit(): void {
    this.affiche();
  }

  affiche() {
    this.donateurService.Afficher().subscribe(res => {
      this.don = res;
    });
  }
  delete(p) {

    this.donateurService.Supprimer(p._id).subscribe(res => {
      this.affiche();
    });
  }
  selectImage(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.image = file;
    }
  }
  onSubmit() {
    const formData = new FormData();
    formData.append('ImageProduct', this.image);

    this.http.post<any>('https://sharek-it-back.herokuapp.com/api/donateur/file', formData).subscribe(res => {
      this.donateur.image = 'assets/img/Donateur/' + res.originalname;
      this.donateurService.Ajouter(this.donateur).subscribe(data => {
        this.affiche();
      });
    });
  }
}
