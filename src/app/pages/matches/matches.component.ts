import { Component, OnInit } from '@angular/core';
import { MatchingService } from 'src/app/_services/matching/matching.service';
import { Matching } from 'src/app/Model/Matching';
import { Router, ActivatedRoute } from '@angular/router';
import { DonService } from 'src/app/_services/Don/don.service';
import { ProfitService } from 'src/app/_services/Profit/profit.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from './modal.component';
import { SnotifyService } from 'ng-snotify';
import { donneur } from 'src/app/Model/Donneur';
import { async } from '@angular/core/testing';
import { forkJoin } from 'rxjs';
import { profit } from 'src/app/Model/ProfitD';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.css']
})
export class MatchesComponent implements OnInit {
  matchs: Matching[] = []
  data = [];
  Fdata =  [];

  settings = {
    actions: { edit: false, add: false },
    delete: {
      confirmDelete: true,

      deleteButtonContent: 'Supprimer',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      etat: {
        title: 'Nom',


      }
    }
  };
  cl
  etat
  profit
  donneur
  ob={}
  constructor(private modalService: NgbModal , private router: Router, private donservice: DonService, private route: ActivatedRoute
    , private profitService: ProfitService, private matchsService: MatchingService , private snotifyService: SnotifyService) {

  }
   q
  ngOnInit(): void {

    this.matchsService.Afficher().subscribe(x => {
      x.forEach( (elem )=> {
        this.cl= elem.code_logistique;
        this.etat = elem.etat;

        forkJoin( this.donservice.RechercherByID(elem.idDon),this.profitService.RechercherByID(elem.idProfit)).subscribe(([res1,res2])=>{
          this.donneur= res1[0] ;
          this.profit=res2[0] ;
        
         this.q = {"_id":elem._id,"donneur":this.donneur,   "profit": this.profit,"etat" : elem.etat, "code_logistique":elem.code_logistique };
         this.data.push(this.q);
          
          
          
        
        
        }); 
        
      }

      );
       /*   this.donservice.RechercherByID(elem.idDon).subscribe(

          a => {
            this.donneur  = a[0];
           this.profitService.RechercherByID(elem.idProfit).subscribe(
              res => {
               
                this.profit  = res[0];
                
                
                console.log(this.data);
            

              }
            );
            
          
          }
        ); */
        /* this.profitService.RechercherByID(elem.idProfit).subscribe(
          res => {this.profit=  res;        
        }
        );
        this.Fdata={"etat": this.etat ,"donneur": this.donneur,"profit": this.profit }  
*/


    

    }
    );


  }
  funct(event: any) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.data = event;
  }
  chnage(dat){
let match =  new Matching;
match._id = dat._id;
match.idDon= dat.donneur._id;
match.idProfit= dat.profit._id;
match.code_logistique = dat.code_logistique;
match.etat = dat.etat;

this.matchsService.Modifier(match).subscribe(res=>{console.log(res);
 
this.snotifyService.success('Success', {
  timeout: 2000,
  showProgressBar: false,
  closeOnClick: false,
  pauseOnHover: true
})},
err=>{
  this.snotifyService.error('Error', {
    timeout: 2000,
    showProgressBar: false,
    closeOnClick: false,
    pauseOnHover: true
  })
}
)



  }
  delete(va){

    this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {text: 'Yes', action: (toast) =>        { 
          this.matchsService.Supprimer(va._id).subscribe(res=>{this.data.splice(this.data.indexOf(va), 1);

          
          this.snotifyService.success('Success', {
          timeout: 2000,
          showProgressBar: false,
          closeOnClick: false,
          pauseOnHover: true
        })},
        err=>{
          this.snotifyService.error('Error', {
            timeout: 2000,
            showProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true
          })
        })
        
        
        ;this.snotifyService.remove(toast.id)}
        , bold: false},
        {text: 'No', action: (toast) =>        {this.snotifyService.remove(toast.id)}},
       
        {text: 'Close', action: (toast) => { this.snotifyService.remove(toast.id); }, bold: true},
      ]
    });
   


  }
}
