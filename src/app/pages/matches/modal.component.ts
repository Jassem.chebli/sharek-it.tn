import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header" >
      <h4 class="modal-title">Fiche</h4>
      <p>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <div class="row">
      <div class="col">
      <h3>Donateur</h3>
      <br>
    <h5>Informations</h5>
    
    Nom : {{data.donneur.nom}} <br>
    Prenom : {{data.donneur.prenom}} <br>
    Age :{{data.donneur.age}} <br> 
    CIN: {{data.donneur.cin}} <br>
    TeL :{{data.donneur.tel}} <br>
    Email :{{data.donneur.Email}} <br> <br>
    <h5>Coordonnées</h5>
    Ville:{{data.donneur.ville}} <br>
    adresse: {{data.donneur.adresse}} <br>
    Code postale :{{data.donneur.codepostal}} <br> <br>
    
    <h5>Produit</h5><br>
       Nom de produit : {{data.donneur.produitName}} <br>
    Detail : {{data.donneur.decriptionProduit}} <br>
    Etat : {{data.donneur.etat}} <br>
    message:{{data.donneur.message}} <br>  
    code Trustit:{{data.donneur.code_trustit}} <br>
    Modele:{{data.donneur.modele}} <br>
    IMEI_SERIAL:{{data.donneur.IMEI_SERIAL}} <br>
    diagnostic:{{data.donneur.diagnostic}} <br>
    <br>
    <img src= "{{ 'https://sharek-it-back.herokuapp.com/' + data.donneur.ImageProduct }}"  class="rounded-top" style = "width:150px;">
    <br>   
     
    </div>
    <div class="col" >
    <h3>Profiteur</h3>
    <br>
    <h5>Informations</h5>
    Nom : {{data.profit.nom}} <br>
    Prenom : {{data.profit.prenom}} <br>
    Age :{{data.profit.age}} <br> 
    CIN: {{data.profit.cin}} <br>
    TeL :{{data.profit.tel}} <br>
    Email :{{data.profit.Email}} <br> 
    etablisement :{{data.profit.etablissement}} <br> 
    Nom du luniversitè : {{data.profit.nom_universite}} <br> 
    specialite   : {{data.profit.specialite }}     <br>    <br> 
    <h5>Coordonnées</h5>
    Ville:{{data.profit.ville}} <br>
    adresse: {{data.profit.adresse}} <br>
    Code postale :{{data.profit.codepostal}} <br> <br>
    
    <h5>Le Besoin</h5>
    
    Le Besoin : {{data.profit.besoin}} <br> 
    Detail du besoin : {{data.profit.detail}}
   <br>
   <br>
    <h2>Statut: {{data.etat}} </h2>

    </div>
    </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() data;
 
  constructor(public activeModal: NgbActiveModal) {
console.log(this.data)


  }
  chnage(va){

    
  }
}