import { Component, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header" >
      <h4 class="modal-title">Fiche</h4>
      <span  class="badge badge-primary" style="float:right;" >{{data.etat}}</span>
      <p>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <h5>Informations</h5>
      Nom : {{data.nom}} <br>
      Prenom : {{data.prenom}} <br>
      Age :{{data.age}} <br> 
      CIN: {{data.cin}} <br>
      TeL :{{data.tel}} <br>
      Email :{{data.Email}} <br> 
      etablisement :{{data.etablissement}} <br> 
      Nom du luniversitè : {{data.nom_universite}} <br> 
      specialite   : {{data.specialite }}     <br>    <br> 
      <h5>Coordonnées</h5>
      Ville:{{data.ville}} <br>
      adresse: {{data.adresse}} <br>
      Code postale :{{data.codepostal}} <br> <br>
      
      <h5>Le Besoin</h5>
      Le Besoin : {{data.besoin}} <br> 
      Detail du besoin : {{data.detail}}
      <br>
      <br>
      <p>montant:</p>
              <input
                matInput
                placeholder="etat du demamnde"
                type="text"
               (keyup)="change()"
                [(ngModel)]="data.etat"
                name="etat"
                required
              />
         
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() data;
   etat
  constructor(public activeModal: NgbActiveModal) {}
  change(){
    console.log(this.etat)
  }
}