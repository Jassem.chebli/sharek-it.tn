import { Component, OnInit, Inject } from '@angular/core';
import { ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { profit } from '../../Model/ProfitD';
import { ProfitService } from '../../_services/Profit/profit.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SnotifyService } from 'ng-snotify';
import { NgbdModalContent } from './modal.component';

import { NgxCsvParser } from 'ngx-csv-parser';
import { NgxCSVParserError } from 'ngx-csv-parser';
import { saveAs } from 'file-saver';
import { AuthService } from 'src/app/_services/auth.service';

import { ExportToCsv } from 'export-to-csv';
import { MatchingService } from 'src/app/_services/matching/matching.service';
import { demandBackup } from 'src/app/Model/demandBackup';
import { DemandBackupService } from 'src/app/_services/demandBackup/demand-backup.service';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { Profit } from 'src/app/Model/profit';

@Component({
  selector: 'app-demands',
  templateUrl: './demands.component.html',
  styleUrls: ['./demands.component.css']
})
export class DemandsComponent implements OnInit {
  demands: profit[] = [];
  settings = {
    actions: { edit: false, add: false },
    delete: {
      confirmDelete: true,

      deleteButtonContent: 'Supprimer',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      besoin: {
        title: 'Besoin',
      },
      Email: {
        title: 'Email',

      },
      etat: {
        title: 'etat',

      },
      tel: {
        title: 'Num tel',
      }
    }
  };

  csvRecords: any[] = [];
  header = false;

  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;

  constructor(private demandService: ProfitService,
    public dialog: MatDialog,
    private profitbackup: DemandBackupService,
    private userService: TokenStorageService,
    private matchsService: MatchingService,
    private donservice: ProfitService,
    private modalService: NgbModal,
    private snotifyService: SnotifyService,
    private ngxCsvParser: NgxCsvParser,
    private authService: AuthService) {

    this.afficherDemands();
  }
  Exportuniv() {
    this.donservice.ExportUniv().subscribe(res => {
      console.log(res);
      saveAs(res, 'instut.xlsx');
    });
  }
  ExportEcole() {
    this.donservice.ExportEcole().subscribe(res => {
      console.log(res);
      saveAs(res, 'ecole.xlsx');
    });
  }
  funct(data) {
    // const modalRef = this.modalService.open(NgbdModalContent);
    // modalRef.componentInstance.data = data.data;
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '40%',
      data: { data: data.data, etat: data.data.etat },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result ) {
        console.log('The dialog was closed ' + result);
        let x = new Profit() ;
     x = data.data
      console.log(x);
          x.etat = result
          this.demandService.Modifier(x).subscribe(res => this.snotifyService.success('Success', {
            timeout: 2000,
            showProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true
          }), err => {
            this.snotifyService.error('Error', {
              timeout: 2000,
              showProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true
            })
          })
        

      }



    });


  }
  ngOnInit(): void { }
  afficherDemands() {
    this.donservice.Afficher().subscribe(res => {
      this.demands = res;
      //   res.forEach(element => {
      //     this.matchsService.RechercherByProfit(element._id as string).subscribe(x => {


      //       let a = "Waiting"

      //       if (x.length > 0) {
      //         a = x[0].etat;
      //         element.statut = a;


      //       } else {
      //         element.statut = a
      //       }


      //     });
      //     this.demands = res;

      //   })
    }
    )

  }

  onDeleteConfirm(event) {


    this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {
          text: 'Yes', action: (toast) => {
            let profitb = new demandBackup
            profitb.adresse = event.data.adresse;
            profitb.age = event.data.age;
            profitb.besoin = event.data.besoin;
            profitb.cin = event.data.cin;
            profitb.codepostal = event.data.codepostal;
            profitb.detail = event.data.detail;
            profitb.etablissement = event.data.etablissement;
            profitb.niveau_scolarite = event.data.niveau_scolarite;
            profitb.nom_universite = event.data.nom_universite;
            profitb.specialite = event.data.specialite;
            profitb.ville = event.data.ville;
            profitb.staut = event.data.statut;
            profitb.tel = event.data.tel;
            profitb.nom = event.data.nom;
            profitb.prenom = event.data.prenom;
            profitb.Email = event.data.Email;
            profitb.id_user = event.data.id_user;


            profitb.user = this.userService.getUser().user._id
            this.profitbackup.Ajouter(profitb).subscribe(res => {


            });




            this.donservice.Supprimer(event.data._id).subscribe(res => {
              this.snotifyService.success('Success', {
                timeout: 2000,
                showProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true
              })
            })


              ; this.snotifyService.remove(toast.id); event.confirm.resolve()
          }
          , bold: false
        },
        { text: 'No', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject() } },

        {
          text: 'Close', action: (toast) => {
            console.log('Clicked: No');
            this.snotifyService.remove(toast.id);
            event.confirm.reject();
          },
          bold: true
        },
      ]
    });

  }


  generateCSV() {

    let data = this.demands;
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    let csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'));
    csv.unshift(header.join(';'));
    let csvArray = csv.join('\r\n');

    var a = document.createElement('a');
    var blob = new Blob([csvArray], { type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = "Demands.csv";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();

    /*

         const options = {
          fieldSeparator: ';',
          quoteStrings: '"',
          decimalSeparator: '.',
          showLabels: true,
          showTitle: false,
          title: 'My Awesome CSV',
          useTextFile: false,
          useBom: true,
          useKeysAsHeaders: true,
          // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
        };

        const csvExporter = new ExportToCsv(options);

        csvExporter.generateCsv(this.demands);

        */
  }


  fileChangeListener($event: any): void {

    let text = [];
    let files = $event.srcElement.files;

    if (this.isCSVFile(files[0])) {

      let input = $event.target;

      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;

        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

        let headersRow = this.getHeaderArray(csvRecordsArray);

        this.csvRecords = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);


        this.saveImportData();
        this.afficherDemands();
      };

      reader.onerror = function () {
        alert('Unable to read ' + input.files[0]);
      };

    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let dataArr = [];


    for (let i = 1; i < csvRecordsArray.length - 1; i++) {
      let data = (<string>csvRecordsArray[i]).split(';');



      // FOR EACH ROW IN CSV FILE IF THE NUMBER OF COLUMNS
      // ARE SAME AS NUMBER OF HEADER COLUMNS THEN PARSE THE DATA
      //if (data.length == headerLength) {
      //console.log('done');
      let csvRecord: profit = new profit();
      csvRecord._id = data[0];
      csvRecord.nom = data[1];
      csvRecord.prenom = data[2];
      csvRecord.age = parseInt(data[3]);
      csvRecord.ville = data[4];
      csvRecord.adresse = data[5];
      csvRecord.tel = data[6];
      csvRecord.Email = data[7];
      csvRecord.codepostal = parseInt(data[8]);
      csvRecord.cin = parseInt(data[9]);
      csvRecord.nom_universite = data[10];
      csvRecord.niveau_scolarite = data[11];
      csvRecord.specialite = data[12];
      csvRecord.etablissement = data[13];
      csvRecord.besoin = data[14];
      csvRecord.id_user = data[15];
      csvRecord.detail = data[16];

      dataArr.push(csvRecord);
      //}
    }
    return dataArr;
  }

  // CHECK IF FILE IS A VALID CSV FILE
  isCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }

  // GET CSV FILE HEADER COLUMNS
  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.fileImportInput.nativeElement.value = "";
    this.csvRecords = [];
  }

  saveImportData() {
    this.csvRecords.forEach(demand => {
      //creation d'un nouveau user est affecter la demand au user
      //console.log(demand)
      let passRandom = Math.floor((Math.random() * 1000000) + 1);
      let user = { username: demand.nom, email: demand.Email, password: 'sh' + passRandom + 'arek' };

      //affect created user to demand
      demand.id_user = this.createUser(user);
      this.donservice.Ajouter(demand).subscribe(data => {
        console.log(data);
        this.afficherDemands();

      });
    });
  }

  createUser(user: any): number {
    this.authService.register(user).subscribe(
      data => {

        this.authService.EmailAccount(user).subscribe(
          res => {

          });
        return data._id;
      },
      err => {
        console.log(err);
      }
    );
    return null;
  }

  replaceAll(ch: String): String {
    ch.replace('"', '');
    if (ch.indexOf('"') != -1)
      this.replaceAll(ch);
    else
      return ch;


  }
}

export interface DialogData {
  data: Profit;
  etat: string;
}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'ModalAngularMAterial.html',
})
export class DialogOverviewExampleDialog {
  etat
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    console.log(data)
  }
  change() {
    console.log(this.etat)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}