import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, MinLengthValidator, FormControl, NgForm } from '@angular/forms';
import { donneur } from '../../Model/Donneur';
import { DonService } from '../../_services/Don/don.service';
import { SnotifyService } from 'ng-snotify';
import { TokenStorageService } from 'src/app/_services/token-storage.service';

import { NgbdModalContent } from './SuccessModal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { product } from 'src/app/Model/product';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';
import { TranslateService } from '@ngx-translate/core';
interface Ville {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-faire-don',
  templateUrl: './faire-don.component.html',
  styleUrls: ['./faire-don.component.css']
})
export class FaireDonComponent implements OnInit {

  loading = false;
  isOptional = false;
  agev = false;
  image: any;
  donneur = new donneur;
  nbr = 1;
  products: product[] = [];
  // image pub logo
  imagePub: any;
  etat: string;
  encapsulation: ViewEncapsulation.None;
  foods: Ville[] = [
    { value: 'Ariana', viewValue: 'Ariana' }, { value: 'Beja', viewValue: 'Beja' }, { value: 'Ben Arous', viewValue: 'Ben Arous' }, { value: 'Bizerte', viewValue: 'Bizerte' },
    { value: 'Gabes', viewValue: 'Gabes' }, { value: 'Gafsa ', viewValue: 'Gafsa ' }, { value: 'Jendouba', viewValue: 'Jendouba' }, { value: 'Kairouan', viewValue: 'Kairouan' },
    { value: 'Kasserine', viewValue: 'Kasserine' }, { value: 'Kebili ', viewValue: 'Kebili ' }, { value: 'Kef ', viewValue: 'Kef ' }, { value: 'Mahdia', viewValue: 'Mahdia' },
    { value: 'Manouba', viewValue: 'Manouba' }, { value: 'Medenine', viewValue: 'Medenine' }, { value: 'Monastir', viewValue: 'Monastir' }, { value: 'Nabeul', viewValue: 'Nabeul' },
    { value: 'Sfax ', viewValue: 'Sfax ' }, { value: 'Sidi Bou Zid ', viewValue: 'Sidi Bou Zid ' }, { value: 'Siliana ', viewValue: 'Siliana ' }, { value: 'Sousse ', viewValue: 'Sousse ' },
    { value: 'Tataouine', viewValue: 'Tataouine' }, { value: 'Tozeur', viewValue: 'Tozeur' }, { value: 'Tunis', viewValue: 'Tunis' }, { value: 'Zaghouan', viewValue: 'Zaghouan' }
  ];
  detailed = false;
  password: any;
  isLoggedIn = true;
  statusClass = '';
  constructor(private modalService: NgbModal, private authService: AuthService, private tokenStorage: TokenStorageService,
    private router: Router, private http: HttpClient, private userService: TokenStorageService,
    private donnerService: DonService, private snotifyService: SnotifyService,
    public translate: TranslateService) {


  }

  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
  }
  ngOnInit() {
    this.products[0] = new product;

    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
    if (this.tokenStorage.getToken()) {

      this.isLoggedIn = false;
    }
  }

  selectImage(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.image = file;
    }
  }
  keyup(event) {
    this.password = event.target.value;
  }
  submitt() {
    const user = { username: this.donneur.nom, email: this.donneur.Email, password: this.password };
    if (!this.tokenStorage.getToken()) {
      this.authService.register(user).subscribe(
        res => {
          this.donneur.id_user = res._id;
          this.submit();
        }
      );
    } else {
      this.donneur.id_user = this.userService.getUser().user._id;
      this.submit();

    }

  }

  handleChange(ev) {

    console.log(ev.source.value);

  }
  selectImagePub(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.imagePub = file;
    }

  }

  submit() {
    if(this.nbr >= 6)
    {
      for (let i = 0; i < this.nbr; i++) {
         this.products[i] = this.products[0];
        }
    }
     this.loading = true;
    // image produit
    const formData = new FormData();
    formData.append('ImageProduct', this.image);
    // image pub
    const formDataPub = new FormData();
    formDataPub.append('ImagePub', this.imagePub);


    if (!this.detailed) {
      if (this.image == undefined) {
        if (this.imagePub == undefined) {
          this.products.forEach(element => {
            this.donneur.produitName = element.produitName;
            this.donneur.etat = element.etat;
            this.donneur.decriptionProduit = element.decriptionProduit;
            
            
            this.donnerService.Ajouter(this.donneur).subscribe(res =>
               this.snotifyService.success('Demande ajoutée avec succès merci beaucoup', {
              timeout: 2000,
              showProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true
            }),
              err =>
                this.snotifyService.error('Error', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                })

            );
            const modalRef = this.modalService.open(NgbdModalContent);
            this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });

          });
          this.router.navigate(['/mesActions']);

        } else {
          console.log('in case');
          this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/filePub', formDataPub).subscribe
            (ress => {
              console.log('case');
              this.donneur.ImagePub = 'assets/img/DonPub/' + ress.originalname;


              this.products.forEach(element => {
                this.donneur.produitName = element.produitName;
                this.donneur.etat = element.etat;
                this.donneur.decriptionProduit = element.decriptionProduit;
                this.donnerService.Ajouter(this.donneur).subscribe(res => this.snotifyService.success('Demande ajoutée avec succès merci beaucoup', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                }),
                  err =>
                    this.snotifyService.error('Error', {
                      timeout: 2000,
                      showProgressBar: false,
                      closeOnClick: false,
                      pauseOnHover: true
                    })
                );
                const modalRef = this.modalService.open(NgbdModalContent);
                this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });
              });
              this.router.navigate(['/mesActions']);
            });
        }
      } else {
        // tslint:disable-next-line: triple-equals
        if (this.imagePub == undefined) {

          this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/file', formData).subscribe
            (res => {
              console.log('t3adet el image ');
              this.donneur.ImageProduct = 'assets/img/Don/' + res.originalname;

              this.products.forEach(element => {
                this.donneur.produitName = element.produitName;
                this.donneur.etat = element.etat;
                this.donneur.decriptionProduit = element.decriptionProduit;
                this.donnerService.Ajouter(this.donneur).subscribe(res => this.snotifyService.success('Demande ajouter avec succès merci beaucoup', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                }),
                  err =>
                    this.snotifyService.error('Error', {
                      timeout: 2000,
                      showProgressBar: false,
                      closeOnClick: false,
                      pauseOnHover: true
                    })

                );

                const modalRef = this.modalService.open(NgbdModalContent);
                this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });
              });
              this.router.navigate(['/mesActions']);


            });
        } else {
          this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/filePub', formDataPub).subscribe
            (ress => {
              this.donneur.ImagePub = 'assets/img/DonPub/' + ress.originalname;
              this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/file', formData).subscribe
                (res => {
                  this.donneur.ImageProduct = 'assets/img/Don/' + res.originalname;



                  this.products.forEach(element => {
                    this.donneur.produitName = element.produitName;
                    this.donneur.etat = element.etat;
                    this.donneur.decriptionProduit = element.decriptionProduit;
                    this.donnerService.Ajouter(this.donneur).subscribe(res => this.snotifyService.success('Demande ajouter avec succès merci beaucoup', {
                      timeout: 2000,
                      showProgressBar: false,
                      closeOnClick: false,
                      pauseOnHover: true
                    }),
                      err =>
                        this.snotifyService.error('Error', {
                          timeout: 2000,
                          showProgressBar: false,
                          closeOnClick: false,
                          pauseOnHover: true
                        })

                    );
                    const modalRef = this.modalService.open(NgbdModalContent);
                    this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });

                  });
                  this.router.navigate(['/mesActions']);



                });

            });



        }


      }




    } else {


      if (this.image == undefined) {
        if (this.imagePub == undefined) {


          for (let i = 0; i < this.nbr; i++) {
            this.donnerService.Ajouter(this.donneur).subscribe(res => {
              this.snotifyService.success('Demande ajouter avec succès merci beaucoup', {
                timeout: 2000,
                showProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true
              });
              this.router.navigate(['/mesActions']);
            },
              err =>
                this.snotifyService.error('Error', {
                  timeout: 2000,
                  showProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true
                })

            );
            const modalRef = this.modalService.open(NgbdModalContent);
            this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });
          }

        } else {

          this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/filePub', formDataPub).subscribe
            (ress => {
              this.donneur.ImagePub = 'assets/img/DonPub/' + ress.originalname;

              for (let i = 0; i < this.nbr; i++) {
                this.donnerService.Ajouter(this.donneur).subscribe(res => {
                  this.snotifyService.success('Demande ajoutée avec succès merci beaucoup', {
                    timeout: 2000,
                    showProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: true
                  });
                  this.router.navigate(['/mesActions']);
                },
                  err =>
                    this.snotifyService.error('Error', {
                      timeout: 2000,
                      showProgressBar: false,
                      closeOnClick: false,
                      pauseOnHover: true
                    })

                );
                const modalRef = this.modalService.open(NgbdModalContent);
                this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });
              }


            });

        }
      } else {
        // nbadalha heka ma temchich 'https://sharek-it-back.herokuapp.com/api/dont/file'

        if (this.imagePub == undefined) {

          this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/file', formData).
            subscribe(res => {

              this.donneur.ImageProduct = 'assets/img/Don/' + res.originalname;

              for (let i = 0; i < this.nbr; i++) {
                this.donnerService.Ajouter(this.donneur).subscribe(res => {
                  this.snotifyService.success('Demande ajoutée avec succès merci beaucoup', {
                    timeout: 2000,
                    showProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: true
                  });
                  this.router.navigate(['/mesActions']);
                },
                  err =>
                    this.snotifyService.error('Error', {
                      timeout: 2000,
                      showProgressBar: false,
                      closeOnClick: false,
                      pauseOnHover: true
                    })
                );
                const modalRef = this.modalService.open(NgbdModalContent);
                this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });
              }
            });
        } else {

          this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/filePub', formDataPub).subscribe
            (ress => {
              this.donneur.ImagePub = 'assets/img/DonPub/' + ress.originalname;
              this.http.post<any>('https://sharek-it-back.herokuapp.com/api/dont/file', formData).subscribe
                (res => {
                  this.donneur.ImageProduct = 'assets/img/Don/' + res.originalname;


                  for (let i = 0; i < this.nbr; i++) {
                    this.donnerService.Ajouter(this.donneur).subscribe(res => {
                      this.snotifyService.success('Demande ajoutée avec succès merci beaucoup', {
                        timeout: 2000,
                        showProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: true
                      });
                      this.router.navigate(['/mesActions']);
                    },
                      err =>
                        this.snotifyService.error('Error', {
                          timeout: 2000,
                          showProgressBar: false,
                          closeOnClick: false,
                          pauseOnHover: true
                        })
                    );
                    const modalRef = this.modalService.open(NgbdModalContent);
                    this.translate.get('notifdon').subscribe(value => { modalRef.componentInstance.data = value; });
                  }
                });
            });
        }
      }
    } 
  }
  toggle() {

    this.detailed = !this.detailed;
  }
  onChange(event) {
    console.log(event);
  }
  sizea() {
    if (this.nbr == 0) {
      this.nbr = 1;
    }else if (this.nbr > 0 && this.nbr < 6) {  
      this.products.length = this.nbr;
      for (let i = 0; i < this.nbr; i++) {
      this.products[i] = new product;
      }
    }
    else if ((this.nbr >= 6)){    
      console.log(this.products.length)
              this.products.forEach(element => {
          this.products.splice(this.products.indexOf(element), this.products.length -1);
        });
      
    }
  }
}
