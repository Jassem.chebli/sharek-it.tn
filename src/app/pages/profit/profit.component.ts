import { Component, OnInit } from '@angular/core';
import { profit } from '../../Model/ProfitD';
import { ProfitService } from '../../_services/Profit/profit.service';
import { SnotifyService } from 'ng-snotify';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { Router } from '@angular/router';
import { UniversityService } from 'src/app/_services/university/university.service';
import { NgbdModalContent } from './SuccessModal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-profit',
  templateUrl: './profit.component.html',
  styleUrls: ['./profit.component.css']
})
export class ProfitComponent implements OnInit {
  profit = new profit;

  univ = 'true'
  universities = []
  ville = ['Monastir', 'Sousse', 'Mehdia'];
  foods: Food[] = [
    { value: 'Ariana', viewValue: 'Ariana' }, { value: 'Beja', viewValue: 'Beja' }, { value: 'Ben Arous', viewValue: 'Ben Arous' }, { value: 'Bizerte', viewValue: 'Bizerte' },
    { value: 'Gabes', viewValue: 'Gabes' }, { value: 'Gafsa ', viewValue: 'Gafsa ' }, { value: 'Jendouba', viewValue: 'Jendouba' }, { value: 'Kairouan', viewValue: 'Kairouan' },
    { value: 'Kasserine', viewValue: 'Kasserine' }, { value: 'Kebili ', viewValue: 'Kebili ' }, { value: 'Kef ', viewValue: 'Kef ' }, { value: 'Mahdia', viewValue: 'Mahdia' },
    { value: 'Manouba', viewValue: 'Manouba' }, { value: 'Medenine', viewValue: 'Medenine' }, { value: 'Monastir', viewValue: 'Monastir' }, { value: 'Nabeul', viewValue: 'Nabeul' },
    { value: 'Sfax ', viewValue: 'Sfax ' }, { value: 'Sidi Bou Zid ', viewValue: 'Sidi Bou Zid ' }, { value: 'Siliana ', viewValue: 'Siliana ' }, { value: 'Sousse ', viewValue: 'Sousse ' },
    { value: 'Tataouine', viewValue: 'Tataouine' }, { value: 'Tozeur', viewValue: 'Tozeur' }, { value: 'Tunis', viewValue: 'Tunis' }, { value: 'Zaghouan', viewValue: 'Zaghouan' }
  ];
  control = new FormControl();
  univs = []
  filteredunivs: Observable<string[]>;

  statusClass = '';
  constructor(private modalService: NgbModal, private universityService: UniversityService,
    private router: Router, private userService: TokenStorageService, private profitSerice: ProfitService,
    private snotifyService: SnotifyService,
    public translate: TranslateService) {

  }
  len = 0;
  ngOnInit(): void {

    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
    this.universityService.Afficher().subscribe(res => {
      this.universities = res;
      res.forEach(element => {
        this.univs.push(element.value)
      });
      this.filt();
    })
  }
  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
  }
  filt() {
    this.filteredunivs = this.control.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }
  private _filter(value: string): string[] {
    const filterValue = this._normalizeValue(value);
    return this.univs.filter(street => this._normalizeValue(street).includes(filterValue));
  }

  private _normalizeValue(value: string): string {
    return value.toString().toLowerCase().replace(/\s/g, '');
  }

  handleChange(ev) {

    console.log(ev.source.value)

  }
  onChange(event) {
    console.log(event);
    //   this.profit.etablissement= event;

  }
  submit() {
    this.profitSerice.Ajouter(this.profit).subscribe(res => {
      this.snotifyService.success('Success', {
        timeout: 2000,
        showProgressBar: false,
        closeOnClick: false,
        pauseOnHover: true
      });
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.data = this.profit.nom;
      this.router.navigate(['/home']);

    },
      err =>
        this.snotifyService.error('Error', {
          timeout: 2000,
          showProgressBar: false,
          closeOnClick: false,
          pauseOnHover: true
        }),

    );



  }
}
