import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DonService } from 'src/app/_services/Don/don.service';
import { donneur } from 'src/app/Model/Donneur';
import { ProfitService } from 'src/app/_services/Profit/profit.service';
import { profit } from 'src/app/Model/ProfitD';
import { SnotifyService } from 'ng-snotify';
import { Matching } from 'src/app/Model/Matching';
import { MatchingService } from 'src/app/_services/matching/matching.service'
import { MatchNService } from 'src/app/_services/MatchN/match-n.service';
import { MatchN } from 'src/app/Model/MatchN';
import { Don } from 'src/app/Model/don';
@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css']
})
export class MatchComponent implements OnInit {
  idDon: string;
  data = new Don;
  demands: profit[] = [];
  matcher = new MatchN;
  profit = new profit;
  test = true;
  settings = {
    actions: { delete: false, edit: false, add: false },
    pager: {
      display: true,
      perPage: 5
    },
    filter: {
      inputClass: 'text-red'
    },
    columns: {
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      besoin: {
        title: 'Besoin',

      },
      Email: {
        title: 'Email',

      }
    }
  };
  panelOpenState = false;

  constructor(private router: Router, private donservice: DonService, private route: ActivatedRoute
    , private profitService: ProfitService, private snotifyService: SnotifyService,
    private matchsService: MatchNService


  ) {



  }

  ngOnInit(): void {
    this.idDon = this.route.snapshot.params.id;
    this.donservice.RechercherByID(this.idDon).subscribe(res => { this.data = res[0]; console.log(res) });
    this.profitService.Afficher().subscribe(res => {
      this.demands = res;
    });
  }
  funct(data) {
    this.profit = data.data;
    this.test = false;
  }
  match() {
    this.matcher.etat = "processing";
    this.matcher.idDon = this.data;
    this.matcher.idProfit = this.profit;
    this.matchsService.Ajouter(this.matcher).subscribe(
      res => this.router.navigate(['/matchs'])
    )

  }
}
