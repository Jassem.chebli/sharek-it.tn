import { Component, OnInit, Inject } from '@angular/core';
import { ViewChild } from '@angular/core';

import { DonService } from 'src/app/_services/Don/don.service';
import { Don } from '../../Model/don';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from './modal.component';
import { SnotifyService } from 'ng-snotify';

import { NgxCsvParser } from 'ngx-csv-parser';
import { NgxCSVParserError } from 'ngx-csv-parser';
import { donneur } from 'src/app/Model/Donneur';
import { MatchingService } from 'src/app/_services/matching/matching.service';
import { DonBackupService } from 'src/app/_services/donBackup/don-backup.service';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { donBackup } from 'src/app/Model/donBackup';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-don',
  templateUrl: './don.component.html',
  styleUrls: ['./don.component.css']
})
export class DonComponent implements OnInit {
  don: donneur[] = [];
  settings = {
    actions: { edit: false, add: false },
    delete: {
      confirmDelete: true,
      deleteButtonContent: 'Supprimer',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      produitName: {
        title: 'Produit',

      },
      Email: {
        title: 'Email',

      },
      statut: {
        title: 'Statut',

      },
      modele: {
        title: 'Modele',

      },
      IMEI_SERIAL: {
        title: 'IMEI_SERIAL',

      },
      etat: {
        title: 'etat',

      },
      diagnostic: {
        title: 'diagnostic',

      },
      tel: {
        title: 'Num tel',

      }
    }
  };

  csvRecords: any[] = [];
  header = false;
  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;

  constructor(private router: Router, public dialog: MatDialog, private donBackup: DonBackupService,
    private userService: TokenStorageService, private matchsService: MatchingService, private donservice: DonService,
    private modalService: NgbModal, private snotifyService: SnotifyService, private ngxCsvParser: NgxCsvParser) {
    this.AfficherDon();
  }


  funct(data) {
    //  const modalRef = this.modalService.open(NgbdModalContent);
    // modalRef.componentInstance.data = data.data;
    // modalRef.componentInstance.data = data.data;

    const dialogRef = this.dialog.open(DialogDons, {
      width: '40%',
      data: { data: data.data, statut: data.data.statut },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        console.log('The dialog was closed ' + result);
        let x = data.data
        if (x.statut != result) {
          console.log(x)
          x.statut = result
          this.donservice.Modifier(x).subscribe(res => this.snotifyService.success('Success', {
            timeout: 2000,
            showProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true
          }), err => {
            this.snotifyService.error('Error', {
              timeout: 2000,
              showProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true
            })
          })
        }

      }




    })

  }

  ngOnInit(): void { }



  AfficherDon() {
    this.donservice.Afficher().subscribe(res => {
      this.don = res;
      //   res.forEach(element => {
      //     this.matchsService.RechercherByDon(element._id as string).subscribe(x => {

      //      let a = "Waiting"

      //       if (x.length > 0) {
      //         a = x[0].etat;
      //         element.statut=a;

      //       }else{
      //         element.statut=a
      //       }


      //     });
      //     this.don = res ; 
      //     this.ngOnInit();
      // })
    }
    );




  }






  onDeleteConfirm(event) {


    this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {
          text: 'Yes', action: (toast) => {


            let donb = new donBackup
            donb.adresse = event.data.adresse;
            donb.age = event.data.age;
            donb.cin = event.data.cin;
            donb.codepostal = event.data.codepostal;
            donb.etat = event.data.etat;
            donb.ville = event.data.ville;
            donb.tel = event.data.tel;
            donb.nom = event.data.nom;
            donb.prenom = event.data.prenom;
            donb.produitName = event.data.produitName;
            donb.decriptionProduit = event.data.decriptionProduit;
            donb.statut = event.data.statut;
            donb.diagnostic = event.data.diagnostic;
            donb.Email = event.data.Email;
            donb.IMEI_SERIAL = event.data.IMEI_SERIAL;
            donb.modele = event.data.modele;
            donb.code_trustit = event.data.code_trustit;
            donb.id_user = event.data.id_user;
            donb.etat = event.data.etat;



            donb.user = this.userService.getUser().user._id
            this.donBackup.Ajouter(donb).subscribe(res => {
              console.log(res);
            });



            this.donservice.Supprimer(event.data._id).subscribe(res => {
              this.snotifyService.success('Success', {
                timeout: 2000,
                showProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true
              })
            })


              ; this.snotifyService.remove(toast.id); event.confirm.resolve()
          }
          , bold: false
        },
        { text: 'No', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject() } },

        { text: 'Close', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject(); }, bold: true },
      ]
    });

  }

  generateCSV() {
    let data = this.don;
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    let csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'));
    csv.unshift(header.join(';'));
    let csvArray = csv.join('\r\n');

    var a = document.createElement('a');
    var blob = new Blob([csvArray], { type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = "Dons.csv";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }


  fileChangeListener($event: any): void {

    let text = [];
    let files = $event.srcElement.files;

    if (this.isCSVFile(files[0])) {

      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;

        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

        let headersRow = this.getHeaderArray(csvRecordsArray);

        this.csvRecords = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);

        this.saveImportData();
      };

      reader.onerror = function () {
        alert('Unable to read ' + input.files[0]);
      };

    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let dataArr = [];

    for (let i = 1; i < csvRecordsArray.length - 1; i++) {
      let data = (<string>csvRecordsArray[i]).split(';');

      // FOR EACH ROW IN CSV FILE IF THE NUMBER OF COLUMNS
      // ARE SAME AS NUMBER OF HEADER COLUMNS THEN PARSE THE DATA
      //if (data.length == headerLength) {

      let csvRecord: donneur = new donneur();

      csvRecord._id = data[0];
      csvRecord.nom = data[1];
      csvRecord.prenom = data[2];
      csvRecord.age = parseInt(data[3]);
      csvRecord.ville = data[4];
      csvRecord.adresse = data[5];
      csvRecord.tel = data[6];
      csvRecord.Email = data[7];
      csvRecord.produitName = data[8];
      csvRecord.decriptionProduit = data[9];
      csvRecord.etat = data[10];
      csvRecord.codepostal = parseInt(data[11]);
      csvRecord.cin = parseInt(data[12]);
      csvRecord.message = data[13];
      csvRecord.ImageProduct = data[14];
      csvRecord.id_user = data[15];



      dataArr.push(csvRecord);
      // }
    }
    return dataArr;
  }

  // CHECK IF FILE IS A VALID CSV FILE
  isCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }

  // GET CSV FILE HEADER COLUMNS
  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.fileImportInput.nativeElement.value = "";
    this.csvRecords = [];
  }

  saveImportData() {
    this.csvRecords.forEach(don => {
      this.donservice.Ajouter(don).subscribe(data => {

        this.AfficherDon();
      })
    });
  }
}
export interface DialogData {
  data: donneur;
  statut: string;
}


@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'ModalAngularMAterial.html',
})
export class DialogDons {

  constructor(private router: Router,
    public dialogRef: MatDialogRef<DialogDons>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    console.log(data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  matchDon() {

    this.router.navigate(['/match', this.data.data._id]);
    this.dialogRef.close();

  }
}