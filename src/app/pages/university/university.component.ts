import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatchingService } from 'src/app/_services/matching/matching.service';
import { ProfitService } from 'src/app/_services/Profit/profit.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SnotifyService } from 'ng-snotify';
import { NgxCsvParser } from 'ngx-csv-parser';
import { profit } from 'src/app/Model/ProfitD';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { NgbdModalContent } from '../demands/modal.component';
import { demandBackup } from 'src/app/Model/demandBackup';
import { DemandBackupService } from 'src/app/_services/demandBackup/demand-backup.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Profit } from 'src/app/Model/profit';
@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.css']
})
export class UniversityComponent implements OnInit {
  demands: profit[] = [];

  _id: string;
  nom: string;
  prenom: string;
  age: number;

  adresse: string;
  codepostal: number;
  ville: string;
  cin: number;
  tel: string;
  Email: string;
  nom_universite: string;
  niveau_scolarite: string;
  specialite: string;
  etablissement: string;
  besoin: string;
  detail: string;
  id_user: string;
  staut: string;
  settings = {
    actions: { edit: false, add: false },
    delete: {
      confirmDelete: true,

      deleteButtonContent: 'Supprimer',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      besoin: {
        title: 'Besoin',

      },
      Email: {
        title: 'Email',

      },
      etat: {
        title: 'Etat',

      }
    }
  };

  csvRecords: any[] = [];
  header = false;

  university_name

  @ViewChild('fileImportInput', { static: false }) fileImportInput: any;

  constructor(private profitbackup: DemandBackupService,
    private userService: TokenStorageService,
    private matchsService: MatchingService,
    private donservice: ProfitService,
    private modalService: NgbModal,
    private snotifyService: SnotifyService
    , private ngxCsvParser: NgxCsvParser,
    private tokenStorageService: TokenStorageService,
    private demandService: ProfitService,
    public dialog: MatDialog,) {
    this.university_name = this.userService.getUser().user.name;
    this.afficherDemands();
  }

  ngOnInit(): void {

  }
  UploadSV() {
    const uni = this.university_name;
    let demand: profit[] = [
      {
        _id: '',
        nom: '',
        prenom: '',
        age: 0,
        ville: '',
        adresse: '',
        tel: '0',
        Email: '',
        codepostal: 0,
        cin: 0,
        nom_universite: uni,
        niveau_scolarite: '',
        specialite: '',
        etablissement: '',
        besoin: '',
        detail: '',
        id_user: '',
        staut: ''
      }
    ];
    let data = demand;
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    let csv = data.map(row =>
      header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'));
    csv.unshift(header.join(';'));
    let csvArray = csv.join('\r\n');

    var a = document.createElement('a');
    var blob = new Blob([csvArray], { type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = "Demands.csv";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();

  }
  afficherDemands() {
    this.donservice.RechercherByUniversity(this.university_name).subscribe(res => {

      res.forEach(element => {
        this.matchsService.RechercherByProfit(element._id as string).subscribe(x => {

          let a = "Waiting"

          if (x.length > 0) {
            a = x[0].etat;
            element.statut = a;


          } else {
            element.statut = a
          }


        });
        this.demands = res;

      })
    },
      () => { this.ngOnInit(); }
    )
  }
  onDeleteConfirm(event) {


    this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {
          text: 'Yes', action: (toast) => {
            let profitb = new demandBackup
            profitb.adresse = event.data.adresse;
            profitb.age = event.data.age;
            profitb.besoin = event.data.besoin;
            profitb.cin = event.data.cin;
            profitb.codepostal = event.data.codepostal;
            profitb.detail = event.data.detail;
            profitb.etablissement = event.data.etablissement;
            profitb.niveau_scolarite = event.data.niveau_scolarite;
            profitb.nom_universite = event.data.nom_universite;
            profitb.specialite = event.data.specialite;
            profitb.ville = event.data.ville;
            profitb.staut = event.data.statut;
            profitb.tel = event.data.tel;
            profitb.nom = event.data.nom;
            profitb.prenom = event.data.prenom;
            profitb.Email = event.data.Email;
            profitb.id_user = event.data.id_user;

            profitb.user = this.userService.getUser().user._id
            this.profitbackup.Ajouter(profitb).subscribe(res => {

            });

            this.donservice.Supprimer(event.data._id).subscribe(res => {
              this.snotifyService.success('Success', {
                timeout: 2000,
                showProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true
              });


            }



            )


              ; this.snotifyService.remove(toast.id); event.confirm.resolve()
          }
          , bold: false
        },
        { text: 'No', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject() } },

        { text: 'Close', action: (toast) => { console.log('Clicked: No'); this.snotifyService.remove(toast.id); event.confirm.reject(); }, bold: true },
      ]
    });

  }


  generateCSV() {

    let data = this.demands;
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    let csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(';'));
    csv.unshift(header.join(';'));
    let csvArray = csv.join('\r\n');

    var a = document.createElement('a');
    var blob = new Blob([csvArray], { type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = "Demands.csv";
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();

    /*
    
         const options = { 
          fieldSeparator: ';',
          quoteStrings: '"',
          decimalSeparator: '.',
          showLabels: true, 
          showTitle: false,
          title: 'My Awesome CSV',
          useTextFile: false,
          useBom: true,
          useKeysAsHeaders: true,
          // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
        };
       
        const csvExporter = new ExportToCsv(options);
        
        csvExporter.generateCsv(this.demands);
    
        */
  }


  fileChangeListener($event: any): void {

    let text = [];
    let files = $event.srcElement.files;

    if (this.isCSVFile(files[0])) {

      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

        let headersRow = this.getHeaderArray(csvRecordsArray);

        this.csvRecords = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);


        this.saveImportData();
      };

      reader.onerror = function () {
        alert('Unable to read ' + input.files[0]);
      };

    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let dataArr = [];


    for (let i = 1; i < csvRecordsArray.length - 1; i++) {
      let data = (<string>csvRecordsArray[i]).split(';');
      // FOR EACH ROW IN CSV FILE IF THE NUMBER OF COLUMNS
      // ARE SAME AS NUMBER OF HEADER COLUMNS THEN PARSE THE DATA
      //if (data.length == headerLength) {
      //console.log('done');
      let csvRecord: profit = new profit();
      csvRecord._id = data[0];
      csvRecord.nom = data[1];
      csvRecord.prenom = data[2];
      csvRecord.age = parseInt(data[3]);
      csvRecord.ville = data[4];
      csvRecord.adresse = data[5];
      csvRecord.tel = data[6];
      csvRecord.Email = data[7];
      csvRecord.codepostal = parseInt(data[8]);
      csvRecord.cin = parseInt(data[9]);
      csvRecord.nom_universite = this.tokenStorageService.getUser().user.name;
      csvRecord.niveau_scolarite = data[11];
      csvRecord.specialite = data[12];
      csvRecord.etablissement = data[13];
      csvRecord.besoin = data[14];
      csvRecord.id_user = data[15];
      csvRecord.detail = data[16];

      dataArr.push(csvRecord);
      //}
    }
    return dataArr;
  }

  // CHECK IF FILE IS A VALID CSV FILE
  isCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }

  // GET CSV FILE HEADER COLUMNS
  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.fileImportInput.nativeElement.value = "";
    this.csvRecords = [];
  }

  saveImportData() {
    this.csvRecords.forEach(demand => {

      //affect created user to demand
      this.donservice.Ajouter(demand).subscribe(data => {

        this.afficherDemands();
      });
    });
  }
  funct(data) {
    // const modalRef = this.modalService.open(NgbdModalContent);
    // modalRef.componentInstance.data = data.data;
    const dialogRef = this.dialog.open(DialogOverviewExampleDialogU, {
      width: '40%',
      data: { data: data.data, etat: data.data.etat },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result ) {
        console.log('The dialog was closed ' + result);
        let x = data.data
       
          x.etat = result
          this.demandService.Modifier(x).subscribe(res => this.snotifyService.success('Success', {
            timeout: 2000,
            showProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true
          }), err => {
            this.snotifyService.error('Error', {
              timeout: 2000,
              showProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true
            })
          })
        

      }



    });


  }


  replaceAll(ch: String): String {
    ch.replace('"', '');
    if (ch.indexOf('"') != -1)
      this.replaceAll(ch);
    else
      return ch;


  }
}
export interface DialogData {
  data: Profit;
  etat: string;
}
@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'ModalAngularMAterial.html',
})
export class DialogOverviewExampleDialogU {
  etat
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialogU>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    console.log(data)
  }
  change() {
    console.log(this.etat)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }

}