import { Component, OnInit } from '@angular/core';
import { donBackup } from 'src/app/Model/donBackup';
import { DonBackupService } from 'src/app/_services/donBackup/don-backup.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from '../don/modal.component';

@Component({
  selector: 'app-don-backup',
  templateUrl: './don-backup.component.html',
  styleUrls: ['./don-backup.component.css']
})
export class DonBackupComponent implements OnInit {
  don: donBackup[] = [];
  settings = {
    actions: { edit: false, add: false, delete: false },
    delete: {

      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      produitName: {
        title: 'Produit',

      },
      Email: {
        title: 'Email',

      },
      statut: {
        title: 'Statut',

      },
      modele: {
        title: 'Modele',

      },
      IMEI_SERIAL: {
        title: 'IMEI_SERIAL',

      },
      etat: {
        title: 'etat',

      },
      diagnostic: {
        title: 'diagnostic',

      },
      tel: {
        title: 'Num tel',

      }
    }
  };
  constructor(private donsBackupervice: DonBackupService, private modalService: NgbModal) { }
  funct(data) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.data = data.data;
    modalRef.componentInstance.data = data.data;
  }
  ngOnInit(): void {
    this.donsBackupervice.Afficher().subscribe(res => {


      this.don = res;

    }
    );
  }

}
