import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DonBackupComponent } from './don-backup.component';

describe('DonBackupComponent', () => {
  let component: DonBackupComponent;
  let fixture: ComponentFixture<DonBackupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DonBackupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DonBackupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
