import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandBackupComponent } from './demand-backup.component';

describe('DemandBackupComponent', () => {
  let component: DemandBackupComponent;
  let fixture: ComponentFixture<DemandBackupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemandBackupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandBackupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
