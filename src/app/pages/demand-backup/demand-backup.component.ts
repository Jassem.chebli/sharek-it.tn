import { Component, OnInit } from '@angular/core';
import { demandBackup } from 'src/app/Model/demandBackup';
import { DemandBackupService } from 'src/app/_services/demandBackup/demand-backup.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from '../demands/modal.component';

@Component({
  selector: 'app-demand-backup',
  templateUrl: './demand-backup.component.html',
  styleUrls: ['./demand-backup.component.css']
})
export class DemandBackupComponent implements OnInit {
  demands: demandBackup[] = [];
  settings = {
    actions: { edit: false, add: false, delete: false },
    delete: {
      confirmDelete: true,

      deleteButtonContent: 'Supprimer',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel'
    }, filter: {
      inputClass: 'text-red'
    },
    columns: {
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      besoin: {
        title: 'Besoin',

      },
      Email: {
        title: 'Email',

      },
      staut: {
        title: 'Statut',

      },
      tel: {
        title: 'Num tel',

      }
    }
  };
  constructor(private demandBackupService: DemandBackupService, private modalService: NgbModal) { }
  funct(data) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.data = data.data;
  }
  ngOnInit(): void {
    this.demandBackupService.Afficher().subscribe(res => this.demands = res)
  }

}
