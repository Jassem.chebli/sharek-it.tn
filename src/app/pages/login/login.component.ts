import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService as AuthenticationService } from '../../_services/auth.service';
import { TokenStorageService } from '../../_services/token-storage.service';
import { Router } from '@angular/router';
/* import { AuthService, FacebookLoginProvider, SocialUser } from 'angularx-social-login'; */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  name: string;
  role: string;
  forgor = false;
  login = true;
  /* user: SocialUser; */
  loggedIn: boolean;
  constructor(
    private authService: AuthenticationService,
    private tokenStorage: TokenStorageService,
    private router: Router,
    /* private fbAuthService: AuthService */
  ) { }


  onSubmit() {
    this.authService.login(this.form).subscribe(
      data => {
        /* this.tokenStorage.saveToken(data); */
        this.tokenStorage.saveUser(data);
        console.log(data)
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.name = this.tokenStorage.getUser().user.name;
        location.reload();
        this.router.navigate(['mesActions']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }
  forgotSubmit(email) {
    this.authService.forgotPassword(email).subscribe(res => {
      localStorage.setItem('user_forgot', JSON.stringify({ mail: res['msg'] }));
      this.router.navigate(["home"]);
    });
  }
  reloadPage() {
    window.location.reload();
  }
  /* signInWithFB(): void {
    this.fbAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  } */
  ngOnInit() {
    if (this.tokenStorage.getToken()) {

      this.isLoggedIn = true;
      this.name = this.tokenStorage.getUser().user.name;
      this.role = this.tokenStorage.getUser().user.role;
      if (this.role.indexOf('user')) {
        this.router.navigate(['userAction']);
      } else if (this.role.indexOf('demandes')) {
        this.router.navigate(['demands']);
      } else if (this.role.indexOf('dons')) {
        this.router.navigate(['don']);
      } else if (this.role.indexOf('user')) {
        this.router.navigate(['userAction']);
      } else if (this.role.indexOf('sponsors')) {
        this.router.navigate(['Coordinateur']);
      } else if (this.role.indexOf('matches')) {
        this.router.navigate(['MatchN']);
      } else if (this.role.indexOf('université')) {
        this.router.navigate(['Université']);
      } else if (this.role.indexOf('agent Technique')) {
        this.router.navigate(['agenttechnique']);
      } else if (this.role.indexOf('universities')) {
        this.router.navigate(['universities']);
      } else if (this.role.indexOf('don argents')) {
        this.router.navigate(['DonsArgents']);
      } else if (this.role.indexOf('blog')) {
        this.router.navigate(['BlogCreator']);
      } else if (this.role.indexOf('admin')) {
        this.router.navigate(['userAction']);
      } else {
        this.router.navigate(['userAction']);
      }
    }
    /* else {
      this.fbAuthService.authState.subscribe((user) => {
        this.user = user;
        this.loggedIn = (user != null);

        this.router.navigate(["mesActions"]);
      });
    } */
  }
  forgotpass() {
    this.forgor = true;
    this.login = false;
  }
  ngOnDestroy() { }
}
