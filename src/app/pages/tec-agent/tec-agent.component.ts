import { Component, OnInit } from '@angular/core';
import { donneur } from 'src/app/Model/Donneur';
import { DonService } from 'src/app/_services/Don/don.service';
import { MatchingService } from 'src/app/_services/matching/matching.service';
import { SnotifyService } from 'ng-snotify';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalContent } from './modalTec';
import { donBackup } from 'src/app/Model/donBackup';
import { DonBackupService } from 'src/app/_services/donBackup/don-backup.service';
import { TokenStorageService } from 'src/app/_services/token-storage.service';
import { MatchNService } from 'src/app/_services/MatchN/match-n.service';


@Component({
  selector: 'app-tec-agent',
  templateUrl: './tec-agent.component.html',
  styleUrls: ['./tec-agent.component.css']
})
export class TecAgentComponent implements OnInit {
  don: donneur[] = [];

  constructor(private donBackup: DonBackupService, private userService: TokenStorageService, private matchsService: MatchNService, private modalService: NgbModal, private donservice: DonService, private snotifyService: SnotifyService) {
    this.AfficherDon();



  }

  settings = {
    actions: { add: false },
    delete: {
      confirmDelete: true,
      deleteButtonContent: 'Delete',
      cancelButtonContent: 'cancel'
    },

    edit: {
      confirmSave: true,
    },
    columns: {
      code_trustit: {
        title: 'Code Trustit',

      },
      nom: {
        title: 'Nom',

      },
      prenom: {
        title: 'Prenom',

      },
      ville: {
        title: 'Ville',

      }, adresse: {
        title: 'adresse',

      },
      modele: {
        title: 'Modele',

      },
      IMEI_SERIAL: {
        title: 'IMEI_SERIAL',

      },
      etat: {
        title: 'etat',

      },
      diagnostic: {
        title: 'diagnostic',

      },
      tel: {
        title: 'Num tel',

      }
    },
  };

  AfficherDon() {
    this.donservice.Afficher().subscribe(res => {

      this.don = res;

    }
    );




  }

  onDeleteConfirm(event) {
    this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {
          text: 'Yes', action: (toast) => {

            let donb = new donBackup
            donb.adresse = event.data.adresse;
            donb.age = event.data.age;
            donb.cin = event.data.cin;
            donb.codepostal = event.data.codepostal;
            donb.etat = event.data.etat;
            donb.ville = event.data.ville;
            donb.tel = event.data.tel;
            donb.nom = event.data.nom;
            donb.prenom = event.data.prenom;
            donb.produitName = event.data.produitName;
            donb.decriptionProduit = event.data.decriptionProduit;
            donb.statut = event.data.statut;
            donb.diagnostic = event.data.diagnostic;
            donb.Email = event.data.Email;
            donb.IMEI_SERIAL = event.data.IMEI_SERIAL;
            donb.modele = event.data.modele;
            donb.code_trustit = event.data.code_trustit;
            donb.id_user = event.data.id_user;
            donb.etat = event.data.etat;



            donb.user = this.userService.getUser().user._id
            this.donBackup.Ajouter(donb).subscribe(res => {
              console.log(res)
            });







            this.donservice.Supprimer(event.data._id).subscribe(res => {
              this.snotifyService.success('Success', {
                timeout: 2000,
                showProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true
              })
            })


              ; this.snotifyService.remove(toast.id); event.confirm.resolve()
          }
          , bold: false
        },
        { text: 'No', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject() } },

        { text: 'Close', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject(); }, bold: true },
      ]
    });

  }

  onCreateConfirm(event) {
    console.log("Create Event In Console")
    console.log(event);

  }

  onSaveConfirm(event) {
    console.log(event.newData)
    this.snotifyService.confirm('Voulez-vous modifier cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {
          text: 'Yes', action: (toast) => {
            this.donservice.Modifier(event.newData).subscribe(res => {
              this.snotifyService.success('Success', {
                timeout: 2000,
                showProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true
              })
            })


              ; this.snotifyService.remove(toast.id); event.confirm.resolve()
          }
          , bold: false
        },
        { text: 'No', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject() } },

        { text: 'Close', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject(); }, bold: true },
      ]
    });
  }
  ngOnInit(): void {
  }

  funct(data) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.data = data.data;
    modalRef.componentInstance.data = data.data;





  }
}
