import { Component, Input , OnInit} from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {Router} from "@angular/router"
import { MatchingService } from 'src/app/_services/matching/matching.service';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header" >
      <h4 class="modal-title">Fiche</h4>
      <span  class="badge badge-primary" style="float:right;" >{{data.statut}}</span>
      <p>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
    <h5>Informations</h5>
      Nom : {{data.nom}} <br>
      Prenom : {{data.prenom}} <br>
      Age :{{data.age}} <br> 
      CIN: {{data.cin}} <br>
      TeL :{{data.tel}} <br>
      Email :{{data.Email}} <br> <br>
      <h5>Coordonnées</h5>
      Ville:{{data.ville}} <br>
      adresse: {{data.adresse}} <br>
      Code postale :{{data.codepostal}} <br> <br>
      
      <h5>Produit</h5><br>
         Nom de produit : {{data.produitName}} <br>
      Detail : {{data.decriptionProduit}} <br>
      Etat : {{data.etat}} <br>
      message:{{data.message}} <br>  
      code Trustit:{{data.code_trustit}} <br>
       Modele:{{data.modele}} <br>
       IMEI_SERIAL:{{data.IMEI_SERIAL}} <br>
       diagnostic:{{data.diagnostic}} <br>

      <br>
      <img src= "{{ 'https://sharek-it-back.herokuapp.com/' + data.ImageProduct }}"  class="rounded-top" style = "width:150px;">
      <br>
     <br> 
    </div>
    <div class="modal-footer">
  

      <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent  implements OnInit {
  @Input() data;
   
  constructor(private router: Router, public activeModal: NgbActiveModal) {


    
  }
  ngOnInit(): void {
  
  }

}