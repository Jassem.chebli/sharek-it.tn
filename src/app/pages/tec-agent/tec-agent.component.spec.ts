import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TecAgentComponent } from './tec-agent.component';

describe('TecAgentComponent', () => {
  let component: TecAgentComponent;
  let fixture: ComponentFixture<TecAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TecAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TecAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
