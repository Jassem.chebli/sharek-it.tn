import { Component, OnInit } from '@angular/core';
import { SnotifyService } from 'ng-snotify';
import { UserService } from 'src/app/_services/User/user.service';
@Component({
  selector: 'app-configurations',
  templateUrl: './configurations.component.html',
  styleUrls: ['./configurations.component.css']
})
export class ConfigurationsComponent implements OnInit {

  event: any;
  users = [];
  showDetail = false;
  detailInfo = {
    id: '',
    name: '',
    email: '',
    role: []
  };
  errorMessage: string;
  settings = {
    delete: {
      deleteButtonContent: '<i class="fas fa-trash btn-sm btn-danger"></i>',
      /* saveButtonContent: '<i class="fas fa-check btn-sm btn-success"></i>',
      cancelButtonContent: '<i class="fas fa-close btn-sm btn-danger"></i>', */
      confirmDelete: true
    },
    columns: {
      name: {
        title: 'name'
      },
      email: {
        title: 'Email',
      },
      role: {
        title: 'Role',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'admin', title: 'Admin' },
              { value: 'user', title: 'User' },
              { value: 'demandes', title: 'Demandes' },
              { value: 'sponsors', title: 'Sponsors' },
            ],
          },
        },
        filterFunction(role?: any, search?: string): boolean {
          console.log(search)
          if (role.indexOf(search) > -1) {

            return true;
          } else {
            return false;
          }
        },
      },
    },
    actions: {
      add: false,
      edit: false,
      delete: true,
      custom: [
        {
          name: 'detail',
          title: '<i class="fas fa-edit btn-sm btn-info"></i>'
        }
      ],
      position: 'right'
    },
    pager: {
      display: true,
      perPage: 10
    },
    attr: {
      class: 'table table-bordered'
    },
  };
  constructor(private snotifyService: SnotifyService, private userservice: UserService) { }


  getUsers() {
    this.userservice.getAll().subscribe(
      data => {
        this.users = data;
      },
      (error) => this.errorMessage = error
    );
  }
  deleteUser(event) {
    this.snotifyService.confirm('Voulez-vous supprimer cette fiche', 'Confirmation', {
      timeout: 5000,
      showProgressBar: true,
      closeOnClick: false,
      pauseOnHover: true,
      buttons: [
        {
          text: 'Yes', action: (toast) => {

            this.userservice.Supprimer(event.data._id).subscribe(res => {
              this.snotifyService.success('Success', {
                timeout: 4000,
                showProgressBar: true,
                closeOnClick: false,
                pauseOnHover: true
              });
            });
            this.snotifyService.remove(toast.id); event.confirm.resolve()
          }
          , bold: false
        },
        { text: 'No', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject(); } },

        { text: 'Close', action: (toast) => { this.snotifyService.remove(toast.id); event.confirm.reject(); }, bold: true },
      ]
    });

  }

  role() {
    this.userservice.role(this.detailInfo).subscribe(res => {
      this.snotifyService.success('Success', {
        timeout: 4000,
        showProgressBar: true,
        closeOnClick: false,
        pauseOnHover: true
      });
      this.event.data.role = [...this.detailInfo.role]
      this.showDetail = false;
    });
  }
  ngOnInit(): void {
    this.getUsers();
  }

  details(event) {
    this.event = event;
    this.showDetail = true;
    this.detailInfo.id = event.data._id;
    this.detailInfo.name = event.data.name;
    this.detailInfo.email = event.data.email;
    this.detailInfo.role = [...event.data.role];
    console.log(this.event.data)

  }
  addrole(role) {
    if (this.detailInfo.role.indexOf(role) > -1) {
      this.detailInfo.role.splice(this.detailInfo.role.indexOf(role), 1);
      if (this.detailInfo.role.length === 0) {
        this.detailInfo.role.push('user');
      }
    } else {
      if (role === 'admin' || role === 'user') {
        this.detailInfo.role = [];
      }
      this.detailInfo.role.push(role);
    }
  }
}
