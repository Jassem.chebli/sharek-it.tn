import { Component, OnInit } from '@angular/core';
import { UniversityService } from 'src/app/_services/university/university.service';
import { University } from 'src/app/Model/University';
import { AuthService } from 'src/app/_services/auth.service';
import { SnotifyService } from 'ng-snotify';
import { HttpClient } from '@angular/common/http';

//import * as Data from './data.json';

@Component({
  selector: 'app-universities',
  templateUrl: './universities.component.html',
  styleUrls: ['./universities.component.css']
})
export class UniversitiesComponent implements OnInit {
  universities: University[] = []
  university = new University;
  password
  da;
  constructor(private http: HttpClient, private authService: AuthService, private universityService: UniversityService, private snotifyService: SnotifyService) { }

  ngOnInit(): void {

    console.log(this.da)

    this.universityService.Afficher().subscribe(res => {
      this.universities = res;
      console.log(res)
    })
  }
  onSubmit() {
    this.university.value = this.university.title;

    this.universityService.Ajouter(this.university).subscribe(res => {
      console.log(res);
      //let passRandom = Math.floor((Math.random() * 1000000) + 1);


      let user = { username: this.university.title, email: this.university.email, password: this.password };

      this.authService.registerUniversity(user).subscribe(res => {
        console.log(res);
        this.universities.push(this.university);
        this.snotifyService.success('Success', {
          timeout: 2000,
          showProgressBar: false,
          closeOnClick: false,
          pauseOnHover: true
        });



      }

      )


    })


  }
  delete(v) {
    this.universityService.Supprimer(v._id).subscribe(res => { this.universities.splice(this.universities.indexOf(v), 1) })


  }


  x = []
  generate() {


    this.da.default.public.forEach(element => {
      let passRandom = Math.floor((Math.random() * 1000000) + 1);
      let e = this.makeid(8) + '@sharek.tn'
      let m = { username: element.name, email: e, password: 'sha' + passRandom + 'rek' };



      let u = new University;
      u.title = element.name;
      u.value = u.title;

      this.universityService.Ajouter(u).subscribe(res => {
        console.log(res);
        //let passRandom = Math.floor((Math.random() * 1000000) + 1);



        this.authService.registerUniversity(m).subscribe(res => console.log(res));
      });
      this.x.push(m);

    });
    console.log(this.x)
    //this.generateCSV()
    this.downloadFile(this.x, 'jsontocsv');
  }




  downloadFile(data, filename = 'data') {
    let csvData = this.ConvertToCSV(data, ['username', 'email', 'password']);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }

  ConvertToCSV(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'S.No;';

    for (let index in headerList) {
      row += headerList[index] + ';';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];

        line += ';' + array[i][head];
      }
      str += line + '\r\n';
    }
    return str;
  }





  makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

}
