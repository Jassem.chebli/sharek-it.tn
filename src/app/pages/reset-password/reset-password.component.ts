import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  form: any = {
    password: '',
    repassword: '',
    email: ''
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = "";
  name: string;
  role: string;
  forgor = false;
  login = true;
  /* user: SocialUser; */
  loggedIn: boolean;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    let dateForgot = new Date();
    //let params = new URLSearchParams(location.search);
    // let time =    params.get('time');
    window.localStorage.setItem('time_forgot', JSON.stringify({ time: dateForgot }));
    setInterval(() => {
      let item = JSON.parse(localStorage.getItem('user_forgot'));
      if (item != null) {
        let date = new Date(dateForgot);
        this.form.email = item.mail;
        console.log(date.getTime());
        let currentDate = new Date();

        if (currentDate.getTime() - date.getTime() > 153047) {
          window.localStorage.removeItem('user_forgot');
          window.localStorage.removeItem('time_forgot');
        }
      }

    }, 1000);
  }
  onSubmit() {
    this.authService.resetPassword(this.form).subscribe(res => {
      console.log(res);
      this.form = {};
      this.router.navigate(["login"]);
      window.localStorage.removeItem('user_forgot');
      window.localStorage.removeItem('time_forgot');
    });
  }
}
