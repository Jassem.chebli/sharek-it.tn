import { Component, OnInit } from '@angular/core';
import { Partenariat } from '../../Model/Partenariat';
import { HttpClient } from '@angular/common/http';
import { PartenariatService } from '../../_services/partenariat/partenariat.service';
@Component({
  selector: 'app-partenariat',
  templateUrl: './partenariat.component.html',
  styleUrls: ['./partenariat.component.css']
})
export class PartenariatComponent implements OnInit {
  partenariat = new Partenariat();
  image: any;
  part: Partenariat[] = [];
  constructor(private http: HttpClient, private partenariatService: PartenariatService) { }

  ngOnInit(): void { this.affiche(); }
  selectImage(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.image = file;
    }
  }
  affiche() {
    this.partenariatService.Afficher().subscribe(res => {
      console.log(res);
      this.part = res;
    });
  }
  delete(p) {
    console.log(p._id);
    this.partenariatService.Supprimer(p._id).subscribe(res => {
      console.log("tfas5et el partenaire");
      this.affiche();
    });
  }
  onSubmit() {
    const formData = new FormData();
    formData.append('ImageProduct', this.image);
    this.http.post<any>('https://sharek-it-back.herokuapp.com/api/partenariat/file', formData).
      subscribe(res => {
        const path2 = res.path.replace(/\\/g, '/');
        this.partenariat.image = 'assets/img/Partenariat/' + res.originalname;
        this.partenariatService.Ajouter(this.partenariat).subscribe(data => {
          this.affiche();
        });
      });
  }


}
