import { Component, OnInit, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from 'src/app/_services/Blog/blog.service';
import { Blog } from 'src/app/Model/Blog';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  Post= new Blog ;
postID
pageId 
isShow: boolean;
statusClass= '';
  topPosToStartShowing = 100;
  constructor(private route: ActivatedRoute, private BlogService:BlogService, public translate: TranslateService) { }

  ngOnInit(): void {
    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }

    this.postID= this.route.snapshot.paramMap.get('id')
    this.pageId="/Post/"+this.postID;
    console.log(this.postID)
    this.BlogService.afficherPost(this.postID).subscribe(res=>{this.Post=res.post;     document.getElementsByTagName("img")[2].style.maxWidth = "100%"
;    console.log(res)});
  }
  @HostListener('window:scroll')
  checkScroll() {
      
    // window의 scroll top
    // Both window.pageYOffset and document.documentElement.scrollTop returns the same result in all the cases. window.pageYOffset is not supported below IE 9.

    const scrollPosition =  document.documentElement.scrollTop || document.body.scrollTop || 0;

    
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }
  
  gotoTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }
  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
  }
}
