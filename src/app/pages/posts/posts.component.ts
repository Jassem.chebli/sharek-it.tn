import { Component, OnInit ,ElementRef, HostListener } from '@angular/core';
import { BlogService } from 'src/app/_services/Blog/blog.service';
import { Blog } from 'src/app/Model/Blog';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent implements OnInit {
  isShow: boolean;
  topPosToStartShowing = 100;

  sum = 8;
  
  constructor(private BlogService: BlogService,
     private spinner: NgxSpinnerService, 
     public translate: TranslateService) { }
  first: Blog
  posts: Blog[]
  postShowed: Blog[] = []
  statusClass = '';
  @HostListener('window:scroll')
  checkScroll() {
      
    // window의 scroll top
    // Both window.pageYOffset and document.documentElement.scrollTop returns the same result in all the cases. window.pageYOffset is not supported below IE 9.

    const scrollPosition =  document.documentElement.scrollTop || document.body.scrollTop || 0;

    
    if (scrollPosition >= this.topPosToStartShowing) {
      this.isShow = true;
    } else {
      this.isShow = false;
    }
  }

  gotoTop() {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
  }
  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
  }

  loadfirsts() {
    this.BlogService.afficherByN(this.first._id).subscribe(res => {
      this.postShowed.push(this.first);
      this.postShowed.push(...res.blogs);
      console.log(this.postShowed)
    }
    )
  }
  ngOnInit(): void {
    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }

    this.BlogService.afficherFirst().subscribe(res => {
      console.log(res);
      this.first = res.blogs; this.loadfirsts()
    })





    this.BlogService.afficherPosts().subscribe(res => {
      this.posts = res.blogs.reverse();

    })

  }
  notscrolly = true
  notEmptyPost = true
  onScroll() {
    if (this.notscrolly && this.notEmptyPost) {
      this.spinner.show();
      this.notscrolly = false;
      const lastPost = this.postShowed[this.postShowed.length - 1];
      const lastPostId = lastPost._id;
      this.appendItems(lastPostId);
    }
  }
  appendItems(lastPostId) {

    this.BlogService.afficherByN(lastPostId).subscribe(res => {
      this.spinner.hide();
      if (res.length === 0) {
        this.notEmptyPost = false;
      }
      this.postShowed.push(...res.blogs);
      this.notscrolly = true;
      console.log(res)
    })

  }

}
