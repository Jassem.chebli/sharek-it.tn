import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MessageService } from '../_services/message/Message.sevice';
import { Router } from '@angular/router';
import { Mail} from '../Model/Mail';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  name: string;
  email: string;
  message: string;
  mail :Mail = new Mail();
  public isCollapsed = true;
  statusClass = '';
  constructor(private router: Router, public translate: TranslateService, private _http: HttpClient, private msgservice: MessageService) {

  }

  processForm() {
     this.mail.Email =  this.email ; 
     this.mail.description = this.message ; 
     this.mail.nom = this.name ; 
     console.log(this.mail); 
   this.msgservice.sendMessage(this.mail).subscribe(
      res =>{
        console.log('HTTP response', res);
        this.email ='';
        this.message = '';
        this.name = '';
      },
      err => console.log('HTTP Error', err),
      () => console.log('HTTP request completed.')
    );
  }

  ngOnInit(): void {
    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
  }
  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
    if (language === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }

  }

}
