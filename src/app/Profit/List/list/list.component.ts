import { Component, OnInit } from '@angular/core';
import { ProfitService } from 'src/app/_services/Profit/profit.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(private profitService : ProfitService) { 
    this.Afficher();
  }

  ngOnInit(): void {
  }

  Afficher(){
    this.profitService.Afficher().subscribe(data =>{
      console.log(data);
    })
  }



}
