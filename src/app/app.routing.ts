import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { HomeComponent } from './home/home.component';
import {
  AuthGuardService as AuthGuard
} from './_services/auth-guard.service';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProfitComponent } from './pages/profit/profit.component';
import { FaireDonComponent } from './pages/faire-don/faire-don.component';
import { ContactComponent } from './contact/contact.component';
import { DonArgentComponent } from './pages/don-argent/don-argent.component';
import { PostComponent } from './pages/post/post.component';
import { PostsComponent } from './pages/posts/posts.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'home',
    redirectTo: '',
    pathMatch: 'full'
  },
  { path: 'about', component: AboutusComponent },
  { path: 'contact', component: ContactComponent },
  {
    path: '',
    data: { roles: ['admin'] },
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        loadChildren:
          './layouts/admin-layout/admin-layout.module#AdminLayoutModule'
      }
    ]
  },
  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      {
        path: '',
        loadChildren:
          './layouts/auth-layout/auth-layout.module#AuthLayoutModule'
      }
    ]
  },
  { path: 'beneficier', component: ProfitComponent },
  { path: 'FaireDon', component: FaireDonComponent },
  { path: 'DonArgent', component: DonArgentComponent },
  { path: 'Post/:id', component: PostComponent },
  { path: 'Posts', component: PostsComponent },
  { path: 'change-password/:currentDateTime' + '?time=' + ':currentDateTime', component: ResetPasswordComponent },
  {
    path: '**',
    redirectTo: ''
  }
  /* error page 404 */
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
