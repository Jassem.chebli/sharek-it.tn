import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from "../../_services/token-storage.service";

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
let RT: RouteInfo[] = [];

if (JSON.parse(localStorage.getItem('auth-user'))) {
  const role = JSON.parse(localStorage.getItem('auth-user'));

  if (role.user.role.indexOf('user') > -1) {
    RT.push({ path: '/userAction', title: 'Home', icon: 'ni-tv-2 text-yellow', class: '' },
      { path: '/mesActions', title: 'Mes Actions', icon: 'ni-circle-08 text-yellow', class: '' });
  }
  if (role.user.role.indexOf('demandes') > -1) {
    RT.push({ path: '/demands', title: 'Demandes', icon: 'ni-archive-2 text-blue', class: '' });
  }
  if (role.user.role.indexOf('dons') > -1) {
    RT.push({ path: '/don', title: 'Dons', icon: 'ni-archive-2 text-info', class: '' });
  }
  if (role.user.role.indexOf('sponsors') > -1) {
    RT.push({ path: '/Coordinateur', title: 'Coordination et support', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/sponsoring', title: 'Partenaire logistique et media ', icon: 'ni-box-2 text-pink', class: '' },
      { path: '/partenariat', title: 'Partenariat et collaboration', icon: 'ni-box-2 text-pink', class: '' },
      { path: '/donateur', title: 'Donateur entreprise', icon: 'ni-archive-2 text-info', class: '' });
  }
  if (role.user.role.indexOf('matches') > -1) {
    RT.push({ path: '/MatchN', title: 'Matches', icon: 'ni-archive-2 text-info', class: '' });
  }
  if (role.user.role.indexOf('université') > -1) {
    RT.push({ path: '/university', title: 'Université', icon: 'ni-archive-2 text-info', class: '' });
  }
  if (role.user.role.indexOf('agent Technique') > -1) {
    RT.push({ path: '/agenttechnique', title: 'Agent Technique', icon: 'ni-archive-2 text-info', class: '' });
  }
  if (role.user.role.indexOf('universities') > -1) {
    RT.push({ path: '/universities', title: 'Universities', icon: 'ni-archive-2 text-info', class: '' });
  }
  if (role.user.role.indexOf('don argents') > -1) {
    RT.push({ path: '/DonsArgents', title: 'Dons Argents', icon: 'ni-archive-2 text-info', class: '' },);
  }
  if (role.user.role.indexOf('blog') > -1) {
    RT.push({ path: '/BlogCreator', title: 'New post', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/BlogsAdmin', title: 'Posts', icon: 'ni-archive-2 text-info', class: '' });
  }

  if (role.user.role.indexOf('admin') > -1) {
    RT.push({ path: '/userAction', title: 'Home', icon: 'ni-tv-2 text-yellow', class: '' },
      { path: '/mesActions', title: 'Mes Actions', icon: 'ni-circle-08 text-yellow', class: '' },
      { path: '/Coordinateur', title: 'Coordination et support', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/sponsoring', title: 'Partenaire logistique et media ', icon: 'ni-box-2 text-pink', class: '' },
      { path: '/partenariat', title: 'Partenariat et collaboration', icon: 'ni-box-2 text-pink', class: '' },
      { path: '/donateur', title: 'Donateur entreprise', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/demands', title: 'Demandes', icon: 'ni-archive-2 text-blue', class: '' },
      { path: '/don', title: 'Dons', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/MatchN', title: 'Matches', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/university', title: 'Université', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/agenttechnique', title: 'Agent Technique', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/universities', title: 'Universities', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/DonsBackUp', title: 'Dons BackUp', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/DemandesBackUp', title: 'Demandes BackUp', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/DonsArgents', title: 'Dons Argents', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/BlogCreator', title: 'New post', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/BlogsAdmin', title: 'Posts', icon: 'ni-archive-2 text-info', class: '' },
      { path: '/Configuration', title: 'Configuration', icon: 'ni-archive-2 text-info', class: '' });
  }
}





export const ROUTES: RouteInfo[] = RT;
/* [
  { path: '/userAction', title: 'Home', icon: 'ni-tv-2 text-yellow', class: '' },
  { path: '/mesActions', title: 'Mes Actions', icon: 'ni-circle-08 text-yellow', class: '' },
  { path: '/Coordinateur', title: 'Coordination et support', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/sponsoring', title: 'Partenaire logistique et media ', icon: 'ni-box-2 text-pink', class: '' },
  { path: '/partenariat', title: 'Partenariat et collaboration', icon: 'ni-box-2 text-pink', class: '' },
  { path: '/donateur', title: 'Donateur entreprise', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/demands', title: 'Demandes', icon: 'ni-archive-2 text-blue', class: '' },
  { path: '/don', title: 'Dons', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/MatchN', title: 'Matches', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/university', title: 'Université', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/agenttechnique', title: 'Agent Technique', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/universities', title: 'Universities', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/DonsBackUp', title: 'Dons BackUp', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/DemandesBackUp', title: 'Demandes BackUp', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/DonsArgents', title: 'Dons Argents', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/BlogCreator', title: 'New post', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/BlogsAdmin', title: 'Posts', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/Configuration', title: 'Configuration', icon: 'ni-archive-2 text-info', class: '' },


]; */
/* export const ROUTESUNIVERSITY: RouteInfo[] = [
  { path: '/university', title: 'Université', icon: 'ni-archive-2 text-info', class: '' },
];

export const ROUTEST_A: RouteInfo[] = [
  { path: '/agenttechnique', title: 'Agent Technique', icon: 'ni-archive-2 text-info', class: '' }
];
export const ROUTESM_A: RouteInfo[] = [
  { path: '/demands', title: 'Demands', icon: 'ni-archive-2 text-blue', class: '' },
  { path: '/don', title: 'Dons', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/matchs', title: 'Matchs', icon: 'ni-archive-2 text-info', class: '' }
];
export const ROUTESUser: RouteInfo[] = [
  { path: '/userAction', title: 'Home', icon: 'ni-tv-2 text-yellow', class: '' },
  { path: '/mesActions', title: 'Mes Actions', icon: 'ni-circle-08 text-yellow', class: '' },
];
export const ROUTESC_A: RouteInfo[] = [
  { path: '/Coordinateur', title: 'Coordination et support', icon: 'ni-archive-2 text-info', class: '' },
  { path: '/sponsoring', title: 'Partenaire logistique et media ', icon: 'ni-box-2 text-pink', class: '' },
  { path: '/partenariat', title: 'Partenariat et collaboration', icon: 'ni-box-2 text-pink', class: '' },
  { path: '/donateur', title: 'Donateur entreprise', icon: 'ni-archive-2 text-info', class: '' },
]; */
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;
  username: any;
  avatar: string;
  role: any;
  constructor(private router: Router, private tokenStorageService: TokenStorageService) {

  }

  ngOnInit() {

    this.username = this.tokenStorageService.getUser().user.name;
    this.avatar = 'https://ui-avatars.com/api/?background=f6bf15&color=262626&name=' + this.username;
    this.role = this.tokenStorageService.getUser().user.role;


    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
    });
    /* this.router.navigate(["userAction"]); */
    /* if (this.role === 'university') {
      this.menuItems = ROUTESUNIVERSITY.filter(menuItem => menuItem);
      this.router.events.subscribe((event) => {
        this.isCollapsed = true;
      });
      this.router.navigate(["university"]);
    } else if (this.role === 'at') {
      this.menuItems = ROUTEST_A.filter(menuItem => menuItem);
      this.router.events.subscribe((event) => {
        this.isCollapsed = true;
      });
      this.router.navigate(["agenttechnique"]);
    } else if (this.role === 'am') {
      this.menuItems = ROUTESM_A.filter(menuItem => menuItem);
      this.router.events.subscribe((event) => {
        this.isCollapsed = true;
      });
      this.router.navigate(["demands"]);
    } else if (this.role === 'user') {
      this.menuItems = ROUTESUser.filter(menuItem => menuItem);
      this.router.events.subscribe((event) => {
        this.isCollapsed = true;
      });
      this.router.navigate(["userAction"]);
    } else if (this.role === 'ac') {
      this.menuItems = ROUTESC_A.filter(menuItem => menuItem);
      this.router.events.subscribe((event) => {
        this.isCollapsed = true;
      });
      this.router.navigate(["Coordinateur"]);
    }
    else if (this.role === '') {
      this.menuItems = ROUTES.filter(menuItem => menuItem);
      this.router.events.subscribe((event) => {
        this.isCollapsed = true;
      });
      this.router.navigate(["userAction"]);
    } */

  }
  logout() {
    this.tokenStorageService.signOut();
  }
}
