import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
import { AuthGuardService } from './_services/auth-guard.service';
import { LocationStrategy, Location, PathLocationStrategy, CommonModule } from '@angular/common';
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider } from 'angularx-social-login';

import { NgxCsvParserModule } from 'ngx-csv-parser';
import { NgbdModalContent } from './pages/demands/modal.component';
import { HomeComponent } from './home/home.component';

import { DonComponent } from './pages/don/don.component';
import { HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ListComponent } from './Profit/List/list/list.component';
import { DonService } from './_services/Don/don.service';
import { SponoringComponent } from './pages/sponoring/sponoring.component';
import { SnotifyService, ToastDefaults, SnotifyModule } from 'ng-snotify';
import { MesActionsComponent } from './pages/mes-actions/mes-actions.component';
import { MatchingService } from 'src/app/_services/matching/matching.service';
import { PartenariatComponent } from './pages/partenariat/partenariat.component';
import { CoordinationComponent } from './pages/coordination/coordination.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { DonateurComponent } from './pages/donateur/donateur.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { UniversitiesComponent } from './pages/universities/universities.component';
import { DemoMaterialModule } from './layouts/admin-layout/Materialmodule';
import { BrowserModule } from '@angular/platform-browser';
import { ProfitComponent } from './pages/profit/profit.component';
import { FaireDonComponent } from './pages/faire-don/faire-don.component';
import { ContactComponent } from './contact/contact.component';
import { DonBackupComponent } from './pages/don-backup/don-backup.component';
import { DemandBackupComponent } from './pages/demand-backup/demand-backup.component';
import { NgProgressRouterModule } from 'ngx-progressbar/router';
import { NgProgressModule } from 'ngx-progressbar';
import { UserLayoutComponent } from './layouts/user-layout/user-layout.component';
import { FinAgentComponent } from './pages/fin-agent/fin-agent.component';
import { DonArgentComponent } from './pages/don-argent/don-argent.component';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent } from './shared/footer/footer.component';
import { SignaturePadModule } from 'angular2-signaturepad';
import { BlogCreatorComponent } from './pages/blog-creator/blog-creator.component';
import { QuillModule } from 'ngx-quill';
import { MatchesnewComponent } from './pages/matchesnew/matchesnew.component';
import { PostComponent } from './pages/post/post.component';
import { PostsComponent } from './pages/posts/posts.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgxSpinnerService, NgxSpinnerModule } from 'ngx-spinner';
import { DisqusModule, DISQUS_SHORTNAME } from 'ngx-disqus';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { DisqusComponent } from './pages/disqus/disqus.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
/* const config = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('2577691762482507')
  }
]);
export function provideConfig() {
  return config;
} */
@NgModule({
  imports: [
    NgxSpinnerModule,
    QuillModule.forRoot(),
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    SnotifyModule,
    DemoMaterialModule,
    MatDialogModule,
    InfiniteScrollModule,
    NgxCsvParserModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    /* SocialLoginModule, */
    NgCircleProgressModule.forRoot({
    }),
    NgProgressModule.withConfig({
      spinnerPosition: 'left',
      color: '#f39c12',
      thick: true
    }),
    NgProgressRouterModule,
    SignaturePadModule,
    ShareButtonsModule.withConfig({
      debug: true
    }),
    ShareIconsModule,
    DisqusModule
  ],
  declarations: [
    AppComponent,
    NgbdModalContent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    HomeComponent,
    ListComponent,
    SponoringComponent,
    PartenariatComponent,
    CoordinationComponent,
    AboutusComponent,
    DonateurComponent,
    ProfitComponent,
    FaireDonComponent,
    ContactComponent,
    DonArgentComponent,
    BlogCreatorComponent,
    MatchesnewComponent,
    UserLayoutComponent,
    DonArgentComponent,
    NavbarComponent,
    FooterComponent,
    PostComponent,
    PostsComponent,
    DisqusComponent,
    ResetPasswordComponent
  ],
  providers: [{ provide: JWT_OPTIONS, useValue: JWT_OPTIONS }, Location, { provide: LocationStrategy, useClass: PathLocationStrategy },
    JwtHelperService, AuthGuardService, authInterceptorProviders, DonService, { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
    SnotifyService, { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }, { provide: DISQUS_SHORTNAME, useValue: 'http-sharek-it-tn' }
    /* , { provide: AuthServiceConfig, useFactory: provideConfig } */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
