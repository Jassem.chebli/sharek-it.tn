import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

/* const TOKEN_KEY = 'auth-token'; */
const USER_KEY = 'auth-user';
@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor(private router: Router) { }

  signOut() {
    window.localStorage.clear();
    this.router.navigate(['home']);
  }

  /* public saveToken(token: string) {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
  } */

  public getToken(): string {
    return localStorage.getItem(USER_KEY);
  }
  public saveUser(user) {
    window.localStorage.removeItem(USER_KEY);
    window.localStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser() {
    return JSON.parse(localStorage.getItem(USER_KEY));
  }
}
