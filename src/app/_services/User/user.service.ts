import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/* const Users_API = 'http://localhost:5000/api/users/'; */
const Users_API = 'https://sharek-it-back.herokuapp.com/api/users/';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<any> {
    return this.http.get(Users_API + 'list', httpOptions);
  }

  public Supprimer(id: String): Observable<any> {
    return this.http.delete(Users_API + 'supprimer/' + id, httpOptions);
  }

  public role(user): Observable<any> {
    return this.http.put(
      Users_API + 'role',
      {
        _id: user.id,
        role: user.role
      },
      httpOptions
    );
  }

}
