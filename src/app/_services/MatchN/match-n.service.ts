import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MatchN } from 'src/app/Model/MatchN';
const  MTCHING_API = "https://sharek-it-back.herokuapp.com/api/Matches/";
/* const URL_LOCAL= "http://localhost:5000/api/Matches/" */

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
@Injectable({
  providedIn: 'root'
})
export class MatchNService {
  constructor(private http: HttpClient) { }
  public Afficher(): Observable<any> {
    return this.http.get(MTCHING_API,httpOptions);
    
  } 
  public Supprimer(id : String): Observable<any> {
    return this.http.get(MTCHING_API + "deleteMatching/"+id,httpOptions);
  }
  public Modifier(matching : MatchN): Observable<any> {
    return this.http.put(MTCHING_API + "ModifyMatching/",matching,httpOptions);
  }

  public Ajouter(matching : MatchN): Observable<any> {
    return this.http.post(MTCHING_API + "addMatching",matching,httpOptions);
  }

}
