import { TestBed } from '@angular/core/testing';

import { MatchNService } from './match-n.service';

describe('MatchNService', () => {
  let service: MatchNService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatchNService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
