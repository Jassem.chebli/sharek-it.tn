import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Don } from 'src/app/Model/don';
import { Observable } from 'rxjs';
import { donneur } from 'src/app/Model/Donneur';

const DON_API = 'https://sharek-it-back.herokuapp.com/api/dont/';
/* const DON_API = "http://localhost:5000/api/dont/"; */

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class DonService {

  constructor(private http: HttpClient) { }
  public Afficher(): Observable<any> {
    return this.http.get(DON_API, httpOptions);
  }
  public Ajouter(don: donneur): Observable<any> {
    return this.http.post(DON_API + 'addDon', don, httpOptions);
  }
  public Modifier(don: donneur): Observable<any> {
    console.log(don)
    return this.http.put(DON_API + 'ModifyDon', don, httpOptions);
  }
  public Supprimer(id: String): Observable<any> {
    return this.http.get(DON_API + 'deleteDon/' + id, httpOptions);
  }
  public RechercherByUser(id: String): Observable<any> {
    return this.http.get(DON_API + '/searchByUser/' + id, httpOptions);
  }
  public RechercherByID(id: String): Observable<any> {
    return this.http.get(DON_API + '/search/' + id, httpOptions);
  }
  public ListDonByID(id: String): Observable<any> {
    return this.http.get(DON_API + '/ListDons/' + id, httpOptions);
  }
  public NbrDons(): Observable<any> {
    return this.http.get(DON_API + 'NbrDons', httpOptions);
  }
}
