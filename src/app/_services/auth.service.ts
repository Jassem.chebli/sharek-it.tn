import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenStorageService } from '../_services/token-storage.service';

/* const AUTH_API = "https://sharek-it-back.herokuapp.com/api/users/"; */
const AUTH_API = 'http://localhost:5000/api/users/';
const Auth_Api = 'http://localhost:5000/api/users/reset/';

const USER_KEY = 'auth-user';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private token: TokenStorageService, private http: HttpClient, public jwtHelper: JwtHelperService) { }

  login(credentials): Observable<any> {
    return this.http.post(
      AUTH_API + 'login',
      {
        email: credentials.username,
        password: credentials.password
      },
      httpOptions
    );
  }

  register(user): Observable<any> {
    return this.http.post(
      AUTH_API + 'register',
      {
        name: user.username,
        email: user.email,
        password: user.password,
        password2: user.password
      },
      httpOptions
    );
  }


  registerUniversity(user): Observable<any> {
    return this.http.post(
      AUTH_API + 'registerUniversity',
      {
        name: user.username,
        email: user.email,
        password: user.password,
        password2: user.password
      },
      httpOptions
    );
  }
  public isAuthenticated(): boolean {
    const token = this.token.getUser();
    // Check whether the token is expired and return
    // true or false
    if (token) {
      return !this.jwtHelper.isTokenExpired(token.token);
    } else {
      return false;
    }

  }

  EmailAccount(user: any): Observable<any> {
    return this.http.get(AUTH_API + 'sendAccount/' + user.username + '/' + user.email + '/' + user.password + '', httpOptions);
  }

  Contact(data: any): Observable<any> {
    return this.http.get(AUTH_API + 'contact/' + data.username + '/' + data.email + '/' + data.message + '', httpOptions);
  }
  forgotPassword(email) {
    return this.http.get(Auth_Api + email, httpOptions);
  }

  resetPassword(user): Observable<any> {
    return this.http.post('http://localhost:5000/api/users/updatePassword/',
      {
        email: user.email,
        password: user.password,
        password2: user.repassword
      },
      httpOptions);
  }

  public role(): Observable<any> {
    const token = this.token.getUser();
    return this.http.post(
      AUTH_API + 'userRole',
      {
        id: token.user._id
      },
      httpOptions
    );
  }

}
