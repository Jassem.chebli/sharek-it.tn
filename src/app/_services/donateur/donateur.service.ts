import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Partenariat } from 'src/app/Model/Partenariat';
import { Observable } from 'rxjs';
import { Donateur } from '../../Model/Donateur';
const Donateur_API = 'https://sharek-it-back.herokuapp.com/api/donateur/';
const User_API = 'https://sharek-it-back.herokuapp.com/api/users/';
/* const Donateur_API = 'http://localhost:5000/api/donateur/';
const User_API = 'http://localhost:5000/api/users/'; */
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class DonateurService {
  constructor(private http: HttpClient) { }

  public Ajouter(donateur: Donateur): Observable<any> {
    return this.http.post(Donateur_API + 'ajouter', donateur, httpOptions);
  }

  public Modifier(donateur: Donateur): Observable<any> {
    return this.http.put(Donateur_API + 'modifier', donateur, httpOptions);
  }

  public Supprimer(id: String): Observable<any> {
    return this.http.get(Donateur_API + 'supprimer/' + id, httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(Donateur_API, httpOptions);
  }

  public Rechercher(id: String): Observable<any> {
    return this.http.get(Donateur_API + 'rechercher/' + id, httpOptions);
  }
  public DonatorUsers(): Observable<any> {
    return this.http.get(User_API + 'NbrUsers', httpOptions);
  }

}
