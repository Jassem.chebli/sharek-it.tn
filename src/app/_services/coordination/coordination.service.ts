import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { COORDINATEUR } from 'src/app/Model/coordinateur';
import { Observable } from 'rxjs';
const COORDINATION_API = "https://sharek-it-back.herokuapp.com/api/coordinateur/";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
@Injectable({
  providedIn: 'root'
})
export class CoordinationService {

  constructor(private http: HttpClient) { }


  public Ajouter(Coordinateur: COORDINATEUR): Observable<any> {
    return this.http.post(COORDINATION_API + "ajouter", Coordinateur, httpOptions);
  }

  public Modifier(Coordinateur: COORDINATEUR): Observable<any> {
    return this.http.put(COORDINATION_API + "modifier", Coordinateur, httpOptions);
  }

  public Supprimer(id: String): Observable<any> {
    return this.http.get(COORDINATION_API + "supprimer/" + id, httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(COORDINATION_API, httpOptions);
  }

  public Rechercher(id: String): Observable<any> {
    return this.http.get(COORDINATION_API + 'rechercher/' + id, httpOptions);
  }
}
