import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Matching } from '../../Model/Matching';
const  DONBACKUP_API = "https://sharek-it-back.herokuapp.com/api/dBackup/";
 
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
import { Observable } from 'rxjs';
import { donBackup } from 'src/app/Model/donBackup';
@Injectable({
  providedIn: 'root'
})
export class DonBackupService {



  
  constructor(private http: HttpClient) { }
  
  public Ajouter(donB : donBackup): Observable<any> {
    return this.http.post(DONBACKUP_API + "ajouter",donB,httpOptions);
  }

  public Modifier(matching : donBackup): Observable<any> {
    return this.http.put(DONBACKUP_API + "modifier",matching,httpOptions);
  }

  public Supprimer(id : String): Observable<any> {
    return this.http.get(DONBACKUP_API + "supprimer/"+id,httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(DONBACKUP_API,httpOptions);
  }  
  
  public RechercherByID(id:string): Observable<any> {
    return this.http.get(DONBACKUP_API+'rechercher/'+id,httpOptions);
  }

}
