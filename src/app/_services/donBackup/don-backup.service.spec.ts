import { TestBed } from '@angular/core/testing';

import { DonBackupService } from './don-backup.service';

describe('DonBackupService', () => {
  let service: DonBackupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DonBackupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
