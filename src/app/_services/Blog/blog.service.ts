import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Don } from 'src/app/Model/don';
import { Observable } from 'rxjs';
import { donneur } from 'src/app/Model/Donneur';
import { Blog } from 'src/app/Model/Blog';

const Blog_API = "https://sharek-it-back.herokuapp.com/api/Blog/";
const URL_TEST = "http://localhost:5000/api/Blog/"

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient) { }

  public createPost(blog: Blog): Observable<any> {
    return this.http.post(Blog_API + 'createPost', blog, httpOptions)
  }
  public afficherPost(id): Observable<any> {
    return this.http.get(Blog_API + 'getPost/' + id, httpOptions)
  }
  public afficherPosts(): Observable<any> {
    return this.http.get(Blog_API + 'getBlogs', httpOptions)
  }
  public afficherFirst(): Observable<any> {
    return this.http.get(Blog_API + 'getFirstBlog', httpOptions)
  } public afficherByN(id): Observable<any> {
    return this.http.get(Blog_API + 'getBlogsByN/' + id, httpOptions)
  }
  public supprimer(id): Observable<any> {
    return this.http.delete(Blog_API + 'deletePost/' + id, httpOptions)
  }
}
