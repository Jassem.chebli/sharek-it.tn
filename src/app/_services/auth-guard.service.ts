import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable({ providedIn: 'root' })
export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) { }
  async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const roles = await this.auth.role().toPromise();
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
    /* roles.some(element => {
      if (route.data.role && route.data.role.indexOf(element) > -1) {
        console.log(element)
        permission = true;
      }
    });
    return permission; */

    /*  else {
      roles.some(element => {
        if (route.data.role && route.data.role.indexOf(element) > -1) {
          return true;
        }
      });
      this.router.navigate(['/userAction']);
      return false;

    } */
  }
}
