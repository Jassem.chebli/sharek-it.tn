import { Injectable } from '@angular/core';
import { University } from 'src/app/Model/University';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const UNIVERSITY_API = "https://sharek-it-back.herokuapp.com/api/university/";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UniversityService {
  constructor(private http: HttpClient) { }

  public Ajouter(university: University): Observable<any> {
    return this.http.post(UNIVERSITY_API + "ajouter", university, httpOptions);
  }

  public Modifier(university: University): Observable<any> {
    return this.http.put(UNIVERSITY_API + "modifier", university, httpOptions);
  }

  public Supprimer(id: String): Observable<any> {
    return this.http.delete(UNIVERSITY_API + "supprimer/" + id, httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(UNIVERSITY_API, httpOptions);
  }
  public Rechercher(id: string): Observable<any> {
    return this.http.get(UNIVERSITY_API + 'rechercher/' + id, httpOptions);
  }





}
