import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders } from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

@Injectable({
    providedIn: 'root'
})  
export class MessageService {
    constructor(private _http: HttpClient) { }
    sendMessage(body) {
        return this._http.post('https://sharek-it-back.herokuapp.com/api/mail/sendMail', body,httpOptions);
    }
}
