import { TestBed } from '@angular/core/testing';

import { DonArgentService } from './don-argent.service';

describe('DonArgentService', () => {
  let service: DonArgentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DonArgentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
