import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { DONARGENT } from 'src/app/Model/donArgent';
import { Observable } from 'rxjs';
/* const DONARGENT_API = "http://localhost:5000/api/dontArgent/"; */
const DONARGENT_API = 'https://sharek-it-back.herokuapp.com/api/dontArgent/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class DonArgentService {
  constructor(private http: HttpClient) { }

  public Ajouter(donArgent: DONARGENT): Observable<any> {
    return this.http.post(DONARGENT_API + 'ajouter', donArgent, httpOptions);
  }

  public Modifier(donArgent: DONARGENT): Observable<any> {
    return this.http.put(DONARGENT_API + 'modifier', donArgent, httpOptions);
  }

  public Supprimer(id: String): Observable<any> {
    return this.http.get(DONARGENT_API + 'supprimer/' + id, httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(DONARGENT_API, httpOptions);
  }

  public Rechercher(id: String): Observable<any> {
    return this.http.get(DONARGENT_API + 'rechercher/' + id, httpOptions);
  }

}
