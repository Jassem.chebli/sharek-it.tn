import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Matching } from '../../Model/Matching';
import { demandBackup } from 'src/app/Model/demandBackup';
const  DemandBACKUP_API = "https://sharek-it-back.herokuapp.com/api/deBackup/";


const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};


@Injectable({
  providedIn: 'root'
})
export class DemandBackupService {
  constructor(private http: HttpClient) { }
  
  public Ajouter(donB : demandBackup): Observable<any> {
    return this.http.post(DemandBACKUP_API + "ajouter",donB,httpOptions);
  }

  public Modifier(matching : demandBackup): Observable<any> {
    return this.http.put(DemandBACKUP_API + "modifier",matching,httpOptions);
  }

  public Supprimer(id : String): Observable<any> {
    return this.http.get(DemandBACKUP_API + "supprimer/"+id,httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(DemandBACKUP_API,httpOptions);
  }  
  
  public RechercherByID(id:string): Observable<any> {
    return this.http.get(DemandBACKUP_API+'rechercher/'+id,httpOptions);
  }
}
