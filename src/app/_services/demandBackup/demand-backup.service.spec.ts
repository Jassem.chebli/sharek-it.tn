import { TestBed } from '@angular/core/testing';

import { DemandBackupService } from './demand-backup.service';

describe('DemandBackupService', () => {
  let service: DemandBackupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DemandBackupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
