import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Matching } from '../../Model/Matching';
const  MTCHING_API = "https://sharek-it-back.herokuapp.com/api/matching/";
 
const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MatchingService {
 
  constructor(private http: HttpClient) { }
  
  public Ajouter(matching : Matching): Observable<any> {
    return this.http.post(MTCHING_API + "addMatching",matching,httpOptions);
  }

  public Modifier(matching : Matching): Observable<any> {
    return this.http.put(MTCHING_API + "ModifyMatching/",matching,httpOptions);
  }

  public Supprimer(id : String): Observable<any> {
    return this.http.get(MTCHING_API + "deleteMatching/"+id,httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(MTCHING_API,httpOptions);
  }  
  public RechercherByDon(id:string): Observable<any> {
    return this.http.get(MTCHING_API+'rechercherByIdDon/'+id,httpOptions);
  }
  public RechercherByID(id:string): Observable<any> {
    return this.http.get(MTCHING_API+'rechercher/'+id,httpOptions);
  }
  public RechercherByProfit(id:string): Observable<any> {
    return this.http.get(MTCHING_API+'rechercherIdProfit/'+id,httpOptions);
  }
}
