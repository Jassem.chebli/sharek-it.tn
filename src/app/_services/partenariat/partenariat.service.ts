import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Partenariat } from 'src/app/Model/Partenariat';
import { Observable } from 'rxjs';
const PARTENARIAT_API = "https://sharek-it-back.herokuapp.com/api/partenariat/";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};
@Injectable({
  providedIn: 'root'
})
export class PartenariatService {

  constructor(private http: HttpClient) { }

  public Ajouter(partenariat: Partenariat): Observable<any> {
    return this.http.post(PARTENARIAT_API + "ajouter", partenariat, httpOptions);
  }

  public Modifier(partenariat: Partenariat): Observable<any> {
    return this.http.put(PARTENARIAT_API + "modifier", partenariat, httpOptions);
  }

  public Supprimer(id: String): Observable<any> {
    return this.http.get(PARTENARIAT_API + "supprimer/" + id, httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(PARTENARIAT_API, httpOptions);
  }

  public Rechercher(id: String): Observable<any> {
    return this.http.get(PARTENARIAT_API + 'rechercher/' + id, httpOptions);
  }
}
