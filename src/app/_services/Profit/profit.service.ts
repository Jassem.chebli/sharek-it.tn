import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { profit } from 'src/app/Model/ProfitD';
import { Observable } from 'rxjs';

const PROFIT_API = 'https://sharek-it-back.herokuapp.com/api/profits/';
// const PROFIT_API = "http://localhost:5000/api/profits/";
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class ProfitService {

  constructor(private http: HttpClient) { }

  // tslint:disable-next-line: no-shadowed-variable
  public Ajouter(profit: profit): Observable<any> {
    return this.http.post(PROFIT_API + 'ajouter', profit, httpOptions);
  }

  public Modifier(profit): Observable<any> {
    return this.http.put(PROFIT_API + 'modifier', profit, httpOptions);
  }
  public NbrProfits(): Observable<any> {
    return this.http.get(PROFIT_API + 'NbrDemands', httpOptions);
  }
  public NbrProfitss(): Observable<any> {
    return this.http.get(PROFIT_API + 'NbrDemands', httpOptions);
  }
  public Supprimer(id: String): Observable<any> {
    return this.http.delete(PROFIT_API + 'supprimer/' + id, httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(PROFIT_API, httpOptions);
  }

  public Rechercher(id: String): Observable<any> {
    return this.http.get(PROFIT_API + 'rechercher/' + id, httpOptions);
  }


  public RechercherByUser(id: String): Observable<any> {
    return this.http.get(PROFIT_API + '/rechercherByUser/' + id, httpOptions);
  }
  public RechercherByID(id: String): Observable<any> {
    return this.http.get(PROFIT_API + '/rechercher/' + id, httpOptions);
  }
  public RechercherByUniversity(id: String): Observable<any> {
    return this.http.get(PROFIT_API + 'rechercherByUniversity/' + id, httpOptions);
  }
  public ExportUniv(): Observable<any> {
    return this.http.get(PROFIT_API + 'extracUniversite', {
      responseType: 'blob',
    });
  }

  public ExportEcole(): Observable<any> {
    return this.http.get(PROFIT_API + 'extracEcole', {
      responseType: 'blob',
    });
  }

}
