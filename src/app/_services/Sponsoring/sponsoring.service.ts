import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Sponsoring } from 'src/app/Model/sponsoring';
import { Observable } from 'rxjs';

const SPONSORING_API = "https://sharek-it-back.herokuapp.com/api/sponsorings/";

const httpOptions = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
};

@Injectable({
  providedIn: 'root'
})
export class SponsoringService {

  constructor(private http: HttpClient) { }

  public Ajouter(sponsoring: Sponsoring): Observable<any> {
    return this.http.post(SPONSORING_API + "ajouter", sponsoring, httpOptions);
  }

  public Modifier(sponsoring: Sponsoring): Observable<any> {
    return this.http.put(SPONSORING_API + "modifier", sponsoring, httpOptions);
  }

  public Supprimer(id: String): Observable<any> {
    return this.http.get(SPONSORING_API + "supprimer/" + id, httpOptions);
  }

  public Afficher(): Observable<any> {
    return this.http.get(SPONSORING_API, httpOptions);
  }

  public Rechercher(id: String): Observable<any> {
    return this.http.get(SPONSORING_API + 'rechercher/' + id, httpOptions);
  }


}
