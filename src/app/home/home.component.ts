import {
  AfterViewInit,
  OnDestroy,
  Component,
  OnInit
} from "@angular/core";
/* import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap'; */
import { ProfitService } from '../_services/Profit/profit.service';
import { DonService } from '../_services/Don/don.service';
import { SponsoringService } from '../_services/Sponsoring/sponsoring.service';
import { CoordinationService } from '../_services/coordination/coordination.service';
import { PartenariatService } from '../_services/partenariat/partenariat.service';
import { DonateurService } from '../_services/donateur/donateur.service';
import { Sponsoring } from '../Model/sponsoring';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
})
export class HomeComponent implements OnInit, AfterViewInit, OnDestroy {
  nbProfit = 0;
  nbDon = 0;
  nbDonateur = 0;
  donpercent = 0;
  partenariatCollaboration: any = [];
  coordinationSupport: any = []
  donateurEntreprise: any = [];
  partenaireLogistiqueMedia: any = [];
  statusClass = '';


  public isCollapsed = true;
  constructor(private profitService: ProfitService,
    private donService: DonService,
    private SponsService: SponsoringService,
    private coordinationService: CoordinationService,
    private partenariatService: PartenariatService,
    private donateurService: DonateurService,
    public translate: TranslateService) {

    this.Afficher();
  }

  ngOnInit(): void {
    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
  }
  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
    if (language === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
  }
  toggleBackgroundClass(): void {
    document.querySelector('body').classList.toggle('bg');
  }

  ngAfterViewInit(): void {
    this.toggleBackgroundClass();
  }

  ngOnDestroy(): void {
    this.toggleBackgroundClass();
  }
  Afficher() {
    this.profitService.NbrProfits().subscribe(data => {
      this.nbProfit = data;
    });
    this.donService.NbrDons().subscribe(data => {
      this.nbDon = data;
      this.donpercent = (this.nbDon / 3000) * 100;
    });
    this.partenariatService.Afficher().subscribe(data => {
      this.partenariatCollaboration = data;
    });
    this.coordinationService.Afficher().subscribe(data => {
      this.coordinationSupport = data;
    });
    this.donateurService.Afficher().subscribe(data => {
      this.donateurEntreprise = data;
    });
    this.donateurService.DonatorUsers().subscribe(data => {
      this.nbDonateur = data;
    });
    this.SponsService.Afficher().subscribe(data => {
      this.partenaireLogistiqueMedia = data;
    });
     
  }
}
