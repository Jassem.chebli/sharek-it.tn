import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';
import { AuthGuardService } from './../../_services/auth-guard.service';
import { authInterceptorProviders } from './../../_helpers/auth.interceptor';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// import { ToastrModule } from 'ngx-toastr';

import { UserActionsComponent } from '../../pages/user-actions/user-actions.component';
// import { ToastrModule } from 'ngx-toastr';

import { FaireDonComponent } from '../../pages/faire-don/faire-don.component';
import { DemoMaterialModule } from './Materialmodule';
import { ProfitComponent } from '../../pages/profit/profit.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgbdModalContent } from '../../pages/don/modal.component';
import { JwtHelperService } from '@auth0/angular-jwt';
import { DemandsComponent, DialogOverviewExampleDialog } from '../../pages/demands/demands.component';
import { DonComponent, DialogDons } from '../../pages/don/don.component';
import { MatButtonModule } from '@angular/material/button';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSliderModule } from '@angular/material/slider';
import { ToastDefaults, SnotifyService, SnotifyModule } from 'ng-snotify';
import { MatchComponent } from 'src/app/pages/match/match.component';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MesActionsComponent } from 'src/app/pages/mes-actions/mes-actions.component';
import { MatchesComponent } from 'src/app/pages/matches/matches.component';
import { UniversityComponent, DialogOverviewExampleDialogU } from 'src/app/pages/university/university.component';
import { TecAgentComponent } from 'src/app/pages/tec-agent/tec-agent.component';
import { UniversitiesComponent } from 'src/app/pages/universities/universities.component';
import { DonBackupComponent } from 'src/app/pages/don-backup/don-backup.component';
import { DemandBackupComponent } from 'src/app/pages/demand-backup/demand-backup.component';
import { FinAgentComponent } from 'src/app/pages/fin-agent/fin-agent.component';
import { MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { QuillModule } from 'ngx-quill';
import { BlogsAdminComponent } from 'src/app/pages/blogs-admin/blogs-admin.component';
import { ConfigurationsComponent } from 'src/app/pages/configurations/configurations.component';
@NgModule({
  imports: [

    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule,
    MatSliderModule,
    MatStepperModule,
    TextFieldModule,
    MatButtonModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    Ng2SmartTableModule,
    NgbModule,
    SnotifyModule,
    MatDialogModule,


  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TablesComponent,
    IconsComponent,
    MapsComponent,
    UserActionsComponent,
    NgbdModalContent,
    DemandsComponent,
    DonComponent, MesActionsComponent,
    MatchesComponent,
    MatchComponent,
    UniversityComponent,
    TecAgentComponent,
    UniversitiesComponent,
    DonBackupComponent,
    DemandBackupComponent,
    FinAgentComponent,
    DialogOverviewExampleDialog,
    DialogOverviewExampleDialogU,
    DialogDons,
    BlogsAdminComponent,
    ConfigurationsComponent



  ], entryComponents: [
    DialogOverviewExampleDialog, DialogOverviewExampleDialogU, DialogDons
  ],
  providers: [
    JwtHelperService, AuthGuardService, authInterceptorProviders,
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
  ],
})

export class AdminLayoutModule { }
