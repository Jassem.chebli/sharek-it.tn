import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { TablesComponent } from '../../pages/tables/tables.component';
import { DonComponent } from '../../pages/don/don.component';
import { SponoringComponent } from '../../pages/sponoring/sponoring.component';
import { UserActionsComponent } from '../../pages/user-actions/user-actions.component';
import { FaireDonComponent } from '../../pages/faire-don/faire-don.component';
import { ProfitComponent } from '../../pages/profit/profit.component';
import { AuthGuardService } from 'src/app/_services/auth-guard.service';

import { DemandsComponent } from '../../pages/demands/demands.component';
import { MesActionsComponent } from 'src/app/pages/mes-actions/mes-actions.component';
import { MatchComponent } from 'src/app/pages/match/match.component';
import { PartenariatComponent } from '../../pages/partenariat/partenariat.component';
import { CoordinationComponent } from '../../pages/coordination/coordination.component';
import { MatchesComponent } from 'src/app/pages/matches/matches.component';
import { DonateurComponent } from '../../pages/donateur/donateur.component';
import { UniversityComponent } from 'src/app/pages/university/university.component';
import { TecAgentComponent } from 'src/app/pages/tec-agent/tec-agent.component';
import { UniversitiesComponent } from 'src/app/pages/universities/universities.component';
import { DonBackupComponent } from 'src/app/pages/don-backup/don-backup.component';
import { DemandBackupComponent } from 'src/app/pages/demand-backup/demand-backup.component';
import { DonArgentComponent } from 'src/app/pages/don-argent/don-argent.component';
import { FinAgentComponent } from 'src/app/pages/fin-agent/fin-agent.component';
import { BlogCreatorComponent } from 'src/app/pages/blog-creator/blog-creator.component';
import { MatchesnewComponent } from 'src/app/pages/matchesnew/matchesnew.component';
import { BlogsAdminComponent } from 'src/app/pages/blogs-admin/blogs-admin.component';
import { ConfigurationsComponent } from 'src/app/pages/configurations/configurations.component';
import {
    AuthGuardService as AuthGuard
} from 'src/app/_services/auth-guard.service';
export const AdminLayoutRoutes: Routes = [/*
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'tables', component: TablesComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent }, */


    {
        path: 'userAction', component: UserActionsComponent,
        data: { roles: ['admin', 'user'] },
        canActivate: [AuthGuard]
    },

    {
        path: 'demands', component: DemandsComponent,
        data: { roles: ['admin', 'demandes'] },
        canActivate: [AuthGuard]
    },

    {
        path: 'don', component: DonComponent,
        data: { roles: ['admin', 'dons'] },
        canActivate: [AuthGuard]
    },

    {
        path: 'sponsoring', component: SponoringComponent,
        data: { roles: ['admin', 'sponsors'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'mesActions', component: MesActionsComponent,
        data: { roles: ['admin', 'user'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'match/:id', component: MatchComponent,
        data: { roles: ['admin', 'matches'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'partenariat', component: PartenariatComponent,
        data: { roles: ['admin', 'sponsors'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'Coordinateur', component: CoordinationComponent,
        data: { roles: ['admin', 'sponsors'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'matchs', component: MatchesComponent,
        data: { roles: ['admin', 'matches'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'donateur', component: DonateurComponent,
        data: { roles: ['admin', 'sponsors'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'university', component: UniversityComponent,
        data: { roles: ['admin', 'université'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'agenttechnique', component: TecAgentComponent,
        data: { roles: ['admin', 'agent Technique'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'universities', component: UniversitiesComponent,
        data: { roles: ['admin', 'universities'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'DonsBackUp', component: DonBackupComponent,
        data: { roles: ['admin'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'DemandesBackUp', component: DemandBackupComponent,
        data: { roles: ['admin'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'DonsArgents', component: FinAgentComponent,
        data: { roles: ['admin', 'don argents'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'BlogCreator', component: BlogCreatorComponent,
        data: { roles: ['admin', 'blog'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'Configuration', component: ConfigurationsComponent,
        data: { roles: ['admin'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'MatchN', component: MatchesnewComponent,
        data: { roles: ['admin', 'matches'] },
        canActivate: [AuthGuard]
    },
    {
        path: 'BlogsAdmin', component: BlogsAdminComponent,
        data: { roles: ['admin', 'blog'] },
        canActivate: [AuthGuard]
    }




];
