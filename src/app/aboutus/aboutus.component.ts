import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {
  public isCollapsed = true;
  email: string;
  username: string;
  message: string;
  statusClass = '';
  constructor(private authService: AuthService, public translate: TranslateService) {

  }

  ngOnInit(): void {
    this.translate.addLangs(['en', 'fr', 'ar']);
    if (localStorage.getItem('locale')) {
      const browserLang = localStorage.getItem('locale');
      this.translate.use(browserLang.match(/en|fr|ar/) ? browserLang : 'fr');
    } else {
      localStorage.setItem('locale', 'fr');
      this.translate.setDefaultLang('fr');
    }
    const lang = localStorage.getItem('locale');
    if (lang === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
  }

  toggleBackgroundClass(): void {
    document.querySelector('body').classList.toggle('bg');
  }
  changeLang(language: string) {
    localStorage.setItem('locale', language);
    this.translate.use(language);
    if (language === 'ar') {
      this.statusClass = 'arabic';
    } else {
      this.statusClass = '';
    }
  }
  open(): void {
    document.querySelector('body').classList.toggle('open');
  }

  ngAfterViewInit(): void {
    this.toggleBackgroundClass();
  }

  ngOnDestroy(): void {
    this.toggleBackgroundClass();
  }


  ContactUs() {
    this.authService.Contact({ email: this.email, username: this.username, message: this.message }).subscribe(
      res => {
        console.log(res);
        this.email = ''
        this.username = ''
        this.message = ''
      });
  }

}
